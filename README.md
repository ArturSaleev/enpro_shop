# ENPRO SHOP

#### Интернет магазин

#### Компоненты
- composer require laravel/ui
- composer require laravel/passport
- composer require astrotomic/laravel-translatable


- php artisan ui:auth
- php artisan migrate
- php artisan passport:install

#### Быстрая установка
- composer install
- php artisan passport:key
- php artisan migrate
- php artisan db:seed


#### Тестовый файл менеджер
- http://127.0.0.1:8000/laravel-filemanager/demo
