<?php


namespace App\Helpers;


use Illuminate\Support\Facades\URL;

class AdminAside
{
    public static function menu()
    {
        $active_page = URL::current();
        $data = [
            [
                "title" => "Панель управления",
                "icon" => "fa fa-tachometer-alt",
                "url" => asset('admin/home'),
                "active" => ($active_page == asset('admin/home')),
            ],
            [
                "title" => "Заказы",
                "icon" => "fa fa-cart-arrow-down",
                "url" => asset('admin/order'),
                "active" => ($active_page == asset('admin/order'))
            ],
            [
                "title" => "Каталог",
                "icon" => "fa fa-scroll",
                "url" => "",
                "active" => false,
                "child" => [
                    [
                        "title" => "Продукты",
                        "icon" => "fa fa-file-image",
                        "url" => asset('admin/products'),
                        "active" => ($active_page == asset('admin/products'))
                    ],
                    [
                        "title" => "Категории",
                        "icon" => "fa fa-folder-open",
                        "url" => asset('admin/categories'),
                        "active" => ($active_page == asset('admin/categories'))
                    ],
                   /* [
                        "title" => "Атрибуты",
                        "icon" => "fa fa-receipt",
                        "url" => asset('admin/attributes'),
                        "active" => ($active_page == asset('admin/attributes'))
                    ],*/
                    [
                        "title" => "Бренды",
                        "icon" => "fa fa-copyright",
                        "url" => asset('admin/brands'),
                        "active" => ($active_page == asset('admin/brands'))
                    ],
                    [
                        "title" => "Акции",
                        "icon" => "fa fa-tags",
                        "url" => asset('admin/promotions'),
                        "active" => ($active_page == asset('admin/promotions'))
                    ],
                  /*  [
                        "title" => "Скидки",
                        "icon" => "fa fa-percentage",
                        "url" => asset('admin/sale'),
                        "active" => ($active_page == asset('admin/sale'))
                    ],*/
                ]
            ],
            [
                "title" => "Модули",
                "icon" => "fa fa-puzzle-piece",
                "url" => "",
                "active" => false,
                "child" => [
                    [
                        "title" => "Банеры",
                        "icon" => "fa fa-image",
                        "url" => asset('admin/banners'),
                        "active" => ($active_page == asset('admin/banners'))
                    ],
                    [
                        "title" => "Новости",
                        "icon" => "fa fa-newspaper",
                        "url" => asset('admin/news'),
                        "active" => ($active_page == asset('admin/news'))
                    ],
                    [
                        "title" => "Клиенты",
                        "icon" => "fa fa-users",
                        "url" => asset('admin/clients'),
                        "active" => ($active_page == asset('admin/clients'))
                    ],
                   /* [
                        "title" => "Страницы",
                        "icon" => "fa fa-globe",
                        "url" => asset('admin/pages'),
                        "active" => ($active_page == asset('admin/pages'))
                    ],*/
                ]
            ],

            [
                "title" => "Локализация",
                "icon" => "fa fa-globe-europe",
                "url" => "",
                "active" => false,
                "child" => [
                    [
                        "title" => "Языки",
                        "icon" => "fa fa-language",
                        "url" => asset('admin/language'),
                        "active" => ($active_page == asset('admin/language'))
                    ],
                    [
                        "title" => "Валюта",
                        "icon" => "fa fa-dollar-sign",
                        "url" => asset('admin/currency'),
                        "active" => ($active_page == asset('admin/currency'))
                    ],
                ]
            ],

            [
                "title" => "Настройки",
                "icon" => "fa fa-cog",
                "url" => asset('admin/config'),
                "active" => ($active_page == asset('admin/config'))
            ],
        ];

        return self::setMainActive($data);
    }

    private static function setMainActive($data)
    {
        foreach($data as $i=>$main){
            if(isset($main['child'])){
                $active = false;
                foreach($main['child'] as $child){
                    if($child['active'] == true){
                        $active = true;
                    }
                }
                $data[$i]['active'] = $active;
            }
        }

        return $data;
    }
}
