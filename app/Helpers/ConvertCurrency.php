<?php


namespace App\Helpers;


use App\Models\Currency;
use Illuminate\Support\Facades\Http;

class ConvertCurrency
{
    public function getAllCurrency()
    {
        $url = env('CURRENCY_SERVICE');
        $res = Http::get($url);
        $xml = $res->body();
        $data = $this->xmlToArray($xml);
        $data = json_decode(json_encode($data));
        return $data;
    }

    public function beautifullyCode($data)
    {
        $res = [];
        foreach($data->Valute as $ds){
            $nominal = $ds->Nominal;
            $value = $ds->Value;
            $res[] = [
                "code" => $ds->CharCode,
                "name" => $ds->Name,
                "rate" => $this->calcCurrency($nominal, $value)
            ];
        }
        return $res;
    }

    private function xmlToArray($xmlstring){
        $xml = simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        return $array;
    }

    public function start()
    {
        $data = (array) $this->getAllCurrency();
        $res_text = 'Дата обновления: '.$data['@attributes']->Date."<br>\n";

        //$def_currency = Currency::query()->where('on_default', true)->first();
        $other_currency = Currency::query()->where('on_default', false)->get();

        $cur_rate = [];
        foreach($data['Valute'] as $ds){
            $cur_rate[$ds->CharCode] = [
                "nominal" => $ds->Nominal,
                "rate" => $ds->Value
            ];
        }

        foreach($other_currency as $value){
            $id = $value->id;
            if(isset($cur_rate[$value->code])) {
                $calc = $this->calcCurrency($cur_rate[$value->code]['nominal'], $cur_rate[$value->code]['rate']);
                $cur = Currency::find($id);
                if($cur) {
                    $cur->rate = $calc;
                    $cur->save();
                    $res_text .= "Валюта $value->code обновлена успешно!<br>\n";
                }
            }else{
                $res_text .= "Валюта $value->code не найдена в сервисе!<br>\n";
            }
        }

        return $res_text;
    }

    public function calcCurrency($nominal, $value)
    {
        return round($nominal / floatval($value), 8);
    }


}
