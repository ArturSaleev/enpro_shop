<?php


namespace App\Helpers;


use App\Models\Attribute;
use App\Models\Brand;
use App\Models\Category;
use App\Models\RefLength;
use App\Models\RefWeight;
use Illuminate\Support\Facades\App;

class Referency
{
    public function ref_length(int $id = 0)
    {
        if($id == 0)
            return RefLength::translated()->get();

        return RefLength::translated()->find($id);
    }

    public function ref_weight(int $id = 0)
    {
        if($id == 0)
            return RefWeight::translated()->get();

        return RefWeight::translated()->find($id);
    }

    public function all_brands()
    {
        return Brand::all();
    }

    public function  all_category()
    {
        return Category::all();
    }

    public function all_attributes()
    {
        return Attribute::with(['attributeParams' => function($q){
            $q->where('locale', App::getLocale());
        }])->get();
    }
}
