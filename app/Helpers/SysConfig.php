<?php


namespace App\Helpers;


use App\Models\About;
use App\Models\Locale;
use Illuminate\Support\Facades\App;

class SysConfig
{
    public static function AllLanguage()
    {
        return Locale::all();
    }

    public static function setDefaultLanguage()
    {
        try {
            $locale = Locale::query()->where('on_default', true)->first();
            App::setLocale($locale->code);
        }catch (\Exception $e){
            return 'ru';
        }
    }

    public static function getAboutText($locale = 'ru')
    {
        try {
            $q = About::query()->where('locale', $locale)->first();
            return $q->content;
        }catch (\Exception $e){
            return '';
        }
    }
}
