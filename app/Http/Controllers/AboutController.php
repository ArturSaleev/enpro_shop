<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Client;
use App\Models\News;
use App\Models\Product;
use App\Models\Promotion;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    protected $config;

    /**
     * Create a new DonorsController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $footer = new ShowFooter();
        $this->config = $footer->config();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $promotions = Promotion::all();
        $news = News::all();
        $clients = Client::inRandomOrder()->take(6)->get();
        $about = About::latest()->first();
        foreach ($this->config->translation()->get() as $var) $addresses[] = $var->address;
        return view(
            'about',
            [
                'about' => $about,
                'products' => $products,
                'promotions' => $promotions,
                'news' => $news,
                'clients' => $clients,
                'footer' => $this->config,
                'addresses' => $addresses,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\About  $about
     * @return \Illuminate\Http\Response
     */
    public function show(About $about)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\About  $about
     * @return \Illuminate\Http\Response
     */
    public function edit(About $about)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\About  $about
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, About $about)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\About  $about
     * @return \Illuminate\Http\Response
     */
    public function destroy(About $about)
    {
        //
    }
}
