<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\SysConfig;
use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\AttributeParam;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;

class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = Attribute::translated()->get();
        $params = $data->toArray();
        foreach($data as $i=>$attr) {
            $attributeParams = $attr->attributeParams->where('locale', App::getLocale());
            $params[$i]['params'] = '';
            foreach($attributeParams as $t=>$attributeParam){
                $params[$i]['params'] .= ($t > 0) ? ', ' : '';
                $params[$i]['params'] .= $attributeParam->name;
            }
        }

        return view('admin.attribute.main', ["data" => $params]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.attribute.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $params = $request->params;
        $data = $request->all();
        unset($data['params']);
        if($request->has('on_view')){
            $data['on_view'] = 1;
        }

        $attr = Attribute::create($data);
        $id = $attr->id;
        foreach($params as $locale=>$param){
            foreach($param as $pr) {
                $ap = new AttributeParam();
                $ap->locale = $locale;
                $ap->attribute_id = $id;
                $ap->name = $pr;
                $ap->save();
            }
        }

        return Redirect::to(route('admin.attributes.index'))->with('message', 'Сохранение прошло успешно!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(int $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(int $id)
    {
        $data = Attribute::translated()->find($id);
        $params = [];
        foreach(SysConfig::AllLanguage() as $lang){
            $params[$lang->code] = AttributeParam::query()
                ->where(['locale'=> $lang->code, "attribute_id" => $id])
                ->get();
        }

        //dd($data, $params);
        return view('admin.attribute.edit', ["data" => $data, "params" => $params]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id)
    {
        $params = $request->params;
        $data = $request->all();
        unset($data['params']);
        if($request->has('on_view')){
            $data['on_view'] = 1;
        }

        $attr = Attribute::find($id);
        $attr->update($data);

        AttributeParam::query()->where('attribute_id', $id)->delete();
        foreach($params as $locale=>$param){
            foreach($param as $pr) {
                $ap = new AttributeParam();
                $ap->locale = $locale;
                $ap->attribute_id = $id;
                $ap->name = $pr;
                $ap->save();
            }
        }

        return Redirect::to(route('admin.attributes.index'))->with('message', 'Сохранение прошло успешно!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        $attr = Attribute::find($id);
        AttributeParam::query()->where('attribute_id', $id)->delete();
        $attr->deleteTranslations();
        $attr->delete();
        return Redirect::to(route('admin.attributes.index'))->with('message', 'Удаление прошло успешно');
    }
}
