<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Banners;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = Banners::translated()->paginate(env('PAGINATION_SIZE'));
        foreach ($data as $url) $url->image = asset($url->image);
        return view('admin.banners.main', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $b = new Banners();
        $langs = \App\Helpers\SysConfig::AllLanguage();
        $translations = [];
        foreach ($langs as $lang) {
            $translations[$lang->code] = [
                'title' => $request->get($lang->code)['title'],
                'content' => $request->get($lang->code)['content']
            ];
        }
        $b->fill($translations);
        $active = (count($request->post('active'))===2)?1:0;
        if($active) {
            Banners::where('active', 1)->update(['active' => 0]);
        }
        $b->active = (bool) $active;
        $b->save();
        $b->refresh();
        if($request->image_banner){
            $imageName = Str::random(40).".".$request->file("image_banner")->extension();
            $path = 'banner/'.$b->id;
            $fullpath = 'data/banner/'.$b->id . '/' . $imageName;
            $file =   Storage::disk('uploads')->files($path);
            Storage::disk('uploads')->delete($file);
            $request->file("image_banner")->storeAs($path, $imageName, 'uploads');
            $b->image = $fullpath;
            $b->save();
        }
        return Redirect::to(route('admin.banners.index'))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(int $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(int $id)
    {
        $data = Banners::translated()->find($id);
        $langs = \App\Helpers\SysConfig::AllLanguage();
        $translations = [];
        foreach ($langs as $lang) {
            $translations[$lang->code] = [
                'title' => $data->translate($lang->code)->title,
                'content' => $data->translate($lang->code)->content,
            ];
        }
        $data->fill($translations);
        return view('admin.banners.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $b = Banners::find($id);
        $langs = \App\Helpers\SysConfig::AllLanguage();
        $translations = [];
        foreach ($langs as $lang) {
            $translations[$lang->code] = [
                'title' => $request->get($lang->code)['title'],
                'content' => $request->get($lang->code)['content'],
            ];
        }
        $b->fill($translations);
        $active = (count($request->post('active'))===2)?1:0;
        if($active) {
            Banners::where('active', 1)->update(['active' => 0]);
        }
        $b->active = (bool) $active;
        if($request->image_banner){
            $imageName = Str::random(40).".".$request->file("image_banner")->extension();
            $path = 'banner/'.$id;
            $fullpath = 'data/banner/'.$id . '/' . $imageName;
            $file =   Storage::disk('uploads')->files($path);
            Storage::disk('uploads')->delete($file);
            $request->file("image_banner")->storeAs($path, $imageName, 'uploads');
            $b->image = $fullpath;
        }
        $b->save();
        return Redirect::to(route('admin.banners.index'))->with('message', 'Сохранение прошло успешно!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        $c = Banners::find($id);
        $folder_path = public_path('data/banner/'.$c->id);
        if (file_exists($folder_path)) {
            array_map('unlink', glob("$folder_path/*.*"));
            rmdir($folder_path);
        }
        $c->delete();
        return Redirect::to(route('admin.banners.index'))->with('message', 'Удаление прошло успешно!');
    }
}
