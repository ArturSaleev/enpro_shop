<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Brand::translated()->paginate(env('PAGINATION_SIZE'));
        foreach ($data as $url) $url->image = asset($url->image);
        return view('admin.brands.main', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $b = Brand::create($request->all());
        if($request->image_bran){
            $imageName = Str::random(40).".".$request->file("image_bran")->extension();
            $path = 'brands/'.$b->id;
            $fullpath = 'data/brands/'.$b->id . '/' . $imageName;
            $file = Storage::disk('uploads')->files($path);
            Storage::disk('uploads')->delete($file);
            $request->file("image_bran")->storeAs($path, $imageName, 'uploads');
            $b->image = $fullpath;
            $b->save();
        }
        return Redirect::to(route('admin.brands.index'))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Brand::translated()->find($id);
        return view('admin.brands.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = Brand::find($id);
        if($request->image_brand){
            $imageName = Str::random(40).".".$request->file("image_brand")->extension();
            $path = 'brands/'.$id;
            $fullpath = 'data/brands/'.$id . '/' . $imageName;
            $file =   Storage::disk('uploads')->files($path);
            Storage::disk('uploads')->delete($file);
            $request->file("image_brand")->storeAs($path, $imageName, 'uploads');
            $brand->image = $fullpath;
        }
        $brand->update($request->all());
        return Redirect::to(route('admin.brands.index'))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::find($id);
        $folder_path = public_path('data/brands/'.$brand->id);
        if (file_exists($folder_path)) {
            array_map('unlink', glob("$folder_path/*.*"));
            rmdir($folder_path);
        }
        $brand->delete();
        return Redirect::to(route('admin.brands.index'))->with('message', 'Удаление прошло успешно');
    }
}
