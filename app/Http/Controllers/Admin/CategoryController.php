<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = Category::translated()->paginate(env('PAGINATION_SIZE'));
        foreach ($data as $url) $url->image = asset($url->image);
        return view('admin.category.main', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data2 = Category::where("parent_id","=",0)->translated('ru')->get();
        return view('admin.category.create', ['categories' => $data2]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $c = Category::create($request->all());
        if($request->image_cat){
            $imageName = Str::random(40).".".$request->file("image_cat")->extension();
            $path = 'categories/'.$c->id;
            $fullpath = 'data/categories/'.$c->id . '/' . $imageName;
            $file =   Storage::disk('uploads')->files($path);
            Storage::disk('uploads')->delete($file);
            $request->file("image_cat")->storeAs($path, $imageName, 'uploads');
            $c->image = $fullpath;
            $c->save();
        }
        return Redirect::to(route('admin.categories.index'))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(int $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(int $id)
    {
        $data = Category::translated()->find($id);
        $data2 = Category::where("parent_id","=",0)->translated('ru')->get();
        return view('admin.category.edit', ['data' => $data, 'categories' => $data2]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $c = Category::find($id);
        if($request->image_cat){
            $imageName = Str::random(40).".".$request->file("image_cat")->extension();
            $path = 'categories/'.$id;
            $fullpath = 'data/categories/'.$id . '/' . $imageName;
            $file =   Storage::disk('uploads')->files($path);
            Storage::disk('uploads')->delete($file);
            $request->file("image_cat")->storeAs($path, $imageName, 'uploads');
            $c->image = $fullpath;
        }
        $c->update($request->all());
        return Redirect::to(route('admin.categories.index'))->with('message', 'Сохранение прошло успешно!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        $c = Category::find($id);
        $folder_path = public_path('data/categories/'.$c->id);
        if (file_exists($folder_path)) {
            array_map('unlink', glob("$folder_path/*.*"));
            rmdir($folder_path);
        }
        $c->delete();
        return Redirect::to(route('admin.categories.index'))->with('message', 'Удаление прошло успешно!');
    }
}
