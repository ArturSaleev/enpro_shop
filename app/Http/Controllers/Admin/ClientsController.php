<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = Client::translated()->paginate(env('PAGINATION_SIZE'));
        foreach ($data as $url) $url->image = asset($url->image);
        return view('admin.clients.main', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
//        Client::create($request->all());
        $client = new Client();
        $name = $request->get('name');
        $client->name = $name;
        $client->save();
        $logo = $request->file('logo');
        $extension = $logo->extension();
        $imageName = Str::random(40).'_';
        $path = 'clients/'.$client->id;

        $client->image = 'data/'.$path.'/'.$imageName.'.'.$extension;
        $logo->storeAs($path, str_replace('data/'.$path.'/', '', $client->image), 'uploads');
        $client->fill([
            'ru' => $request->get('ru'),
            'en' => $request->get('en')
        ]);
        $client->save();
        return Redirect::to(route('admin.clients.index'))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(int $id)
    {
        $data = Client::translated()->find($id);
        return view('admin.clients.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id)
    {
        $client = Client::find($id);
        $name = $request->get('name');
        $client->name = $name;
        $logo = $request->file('logo');
        if ($logo !== null) {
            file_exists($client->image) && unlink($client->image);
            $extension = $logo->extension();
            $imageName = Str::random(40).'_';
            $path = 'clients/'.$client->id;
            $client->image = 'data/'.$path.'/'.$imageName.'.'.$extension;
            $logo->storeAs($path, str_replace('data/'.$path.'/', '', $client->image), 'uploads');
        }
        $client->fill([
            'ru' => $request->get('ru'),
            'en' => $request->get('en')
        ]);
        $client->save();
        return Redirect::to(route('admin.clients.index'))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        $client = Client::find($id);
        $folder_path = public_path('data/clients/'.$id);
        if (file_exists($folder_path)) {
            array_map('unlink', glob("$folder_path/*.*"));
            rmdir($folder_path);
        }
        $client->delete();
        return Redirect::to(route('admin.clients.index'))->with('message', 'Удаление прошло успешно');
    }
}
