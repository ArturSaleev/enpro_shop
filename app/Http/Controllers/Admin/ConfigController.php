<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\SysConfig;
use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\AboutTranslation;
use App\Models\Config;
use App\Models\ConfigTranslation;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ConfigController extends Controller
{
    protected $blade = 'admin.config';
    protected $route = 'admin.config.index';
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = Config::first();
        $addr = [];
        $lang = SysConfig::AllLanguage();
        if($data) {
            foreach ($lang as $lng) {
                $addr[$lng->code] = ConfigTranslation::query()->where(['config_id' => $data->id, "locale" => $lng->code])->get();
            }
        }
        $about = About::translated()->first();
        return view($this->blade, ['data' => $data, "about" => $about, "addr" => $addr]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return RedirectResponse
     */
    public function create()
    {
        return Redirect::to(route($this->route));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $email = ($request->has('email')) ? $request->post('email') : '';
        $phone = ($request->has('phone')) ? $request->post('phone') : '';
        $address = ($request->has('address')) ? $request->post('address') : [];

        $about_content = ($request->has('about')) ? $request->post('about') : [];

        //Очищаем все таблитцы от всех записей чтобы не было дубликатов
        ConfigTranslation::truncate();
        DB::table('config')->delete();
        About::truncate();
        AboutTranslation::truncate();
        $about = new About();
        $about->fill($about_content['translations']);
        $about->image = $request->about_img_bk;
        $about->save();
        $about->refresh();
        if($request->image_about){
            $imageName = Str::random(40).".".$request->file("image_about")->extension();
            $path = 'about/'.$about->id;
            $fullpath = 'data/about/'.$about->id . '/' . $imageName;
            $file =   Storage::disk('uploads')->files($path);
            Storage::disk('uploads')->delete($file);
            $request->file("image_about")->storeAs($path, $imageName, 'uploads');
            $about->image = $fullpath;
            $about->save();
        }

        $config = new Config();
        $config->email = $email;
        $config->phone = $phone;
        $config->logo = $request->logo_bk;
        $config->icon = $request->icon_bk;
        $config->save();
        $config->refresh();
        $id_config = $config->id;
        if($request->logo){
            $imageName = Str::random(40).".".$request->file("logo")->extension();
            $path = 'config/'.$id_config;
            $fullpath = 'data/config/'.$id_config . '/' . $imageName;
            $file =   Storage::disk('uploads')->files($path);
            $request->file("logo")->storeAs($path, $imageName, 'uploads');
            $config->logo = $fullpath;
            $config->save();
        }
        if($request->icon){
            $imageName = Str::random(40).".".$request->file("icon")->extension();
            $path = 'config/'.$id_config;
            $fullpath = 'data/config/'.$id_config . '/' . $imageName;
            $file =   Storage::disk('uploads')->files($path);
            $request->file("icon")->storeAs($path, $imageName, 'uploads');
            $config->icon = $fullpath;
            $config->save();
        }

        foreach($address as $locale=>$array) {
            foreach ($array as $addr) {
                $ct = new ConfigTranslation();
                $ct->config_id = $id_config;
                $ct->locale = $locale;
                $ct->address = $addr;
                $ct->save();
            }
        }

        return Redirect::to(route($this->route))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function show(int $id)
    {
        return Redirect::to(route($this->route));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function edit(int $id)
    {
        return Redirect::to(route($this->route));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id)
    {
        return Redirect::to(route($this->route))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        return Redirect::to(route($this->route));
    }
}
