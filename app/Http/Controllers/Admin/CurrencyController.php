<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ConvertCurrency;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class CurrencyController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = Currency::query()->get([
            "id",
            "code",
            "name",
            "symbol",
            "rate",
            DB::raw("case when on_default then 'Да' else 'Нет' end as on_default"),
            DB::raw("case when position_left then 'Слева' else 'Справа' end as position_left")
        ]);
        return view('admin.currency.main', ["data" => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $cc = new ConvertCurrency();
        $data = $cc->getAllCurrency();
        $data_currency = $cc->beautifullyCode($data);
        $data_currency_json = json_encode($data_currency);
        return view('admin.currency.create',
            ['data_currency' => $data_currency, 'data_currency_json' => $data_currency_json]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $code = $request->input('code');
        $name = $request->input('name');
        $symbol = $request->input('symbol');
        $rate = $request->input('rate');
        $position = (bool) $request->input('position_left');
        $default = ($request->has('on_default'));

        $cr = new Currency();
        $cr->code = $code;
        $cr->name = $name;
        $cr->symbol = $symbol;
        $cr->rate = $rate;
        $cr->position_left = $position;
        $cr->on_default = $default;
        $cr->save();

        if($default){
            $id = $cr->id;
            Currency::query()->whereKeyNot($id)->update([
                "on_default" => false
            ]);
        }

        return Redirect::to(route('admin.currency.index'))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Currency::find($id);

        $cc = new ConvertCurrency();
        $data_currency = $cc->beautifullyCode($cc->getAllCurrency());
        $data_currency_json = json_encode($data_currency);

        return view('admin.currency.edit', [
            "data" => $data,
            'data_currency' => $data_currency,
            'data_currency_json' => $data_currency_json
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id)
    {
        $code = $request->input('code');
        $name = $request->input('name');
        $symbol = $request->input('symbol');
        $rate = $request->input('rate');
        $position = (bool) $request->input('position_left');
        $default = ($request->has('on_default'));

        $cr = Currency::find($id);
        $cr->code = $code;
        $cr->name = $name;
        $cr->symbol = $symbol;
        $cr->rate = $rate;
        $cr->position_left = $position;
        $cr->on_default = $default;
        $cr->save();

        if($default){
            Currency::query()->whereKeyNot($id)->update([
                "on_default" => false
            ]);
        }

        return Redirect::to(route('admin.currency.index'))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $ls = Currency::all();
        if(count($ls) <= 1){
            return Redirect::to(route('admin.language.index'))
                ->withErrors(["errorMessage" => "Удаление единственного валюты запрещено!"]);
        }

        $c = Currency::find($id);
        if($c->on_default){
            return Redirect::to(route('admin.currency.index'))
                ->withErrors(["errorMessage" => "Удаление основной валюты запрещено!"]);
        }
        $c->delete();
        return Redirect::to(route('admin.currency.index'))->with('message', 'Удаление прошло успешно');
    }


    public function ServiceConvertCurrency()
    {
        $cr = new ConvertCurrency();
        $text = $cr->start();
        return Redirect::to(route('admin.currency.index'))->with('message', $text);
    }
}
