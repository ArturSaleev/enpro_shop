<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Locale;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class LocaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = Locale::all();
        return view('admin.locale.main', ["data" => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.locale.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            "name" => "required",
            "code" => "required"
        ]);
        if($validate->fails()){
            return Redirect::to(route('admin.language.create'))
                ->withErrors($validate);
        }

        $name = $request->post('name');
        $code = $request->post('code');

        $locale = new Locale();
        $locale->name = $name;
        $locale->code = $code;
        $locale->display_name = $name;
        $locale->lang_code = $code;
        if($request->has('image')){
            $locale->image = $request->post('image');
        }
        $locale->on_default = ($request->has('on_default'));
        $locale->saveOrFail();

        return Redirect::to(route('admin.language.index'))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Locale::find($id);
        return view('admin.locale.edit', ["data" => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            "name" => "required",
            "code" => "required"
        ]);
        if($validate->fails()){
            return Redirect::to(route('admin.language.create'))
                ->withErrors($validate);
        }

        $name = $request->post('name');
        $code = $request->post('code');

        $locale = Locale::find($id);
        $locale->name = $name;
        $locale->code = $code;
        $locale->display_name = $name;
        $locale->lang_code = $code;
        if($request->has('image')){
            $locale->image = $request->post('image');
        }
        $locale->on_default = ($request->has('on_default'));
        $locale->saveOrFail();

        return Redirect::to(route('admin.language.index'))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $ls = Locale::all();
        if(count($ls) <= 1){
            return Redirect::to(route('admin.language.index'))
                ->withErrors(["errorMessage" => "Удаление единственного языка запрещено!"]);
        }

        $locale = Locale::find($id);

        if($locale->on_default){
            return Redirect::to(route('admin.language.index'))
                ->withErrors(["errorMessage" => "Удаление основного языка запрещено!"]);
        }

        $locale->delete();
        return Redirect::to(route('admin.language.index'))->with('message', 'Удаление прошло успешно');
    }
}
