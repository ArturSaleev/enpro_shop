<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Banners;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Client;
use App\Models\News;
use App\Models\Order;
use App\Models\Product;
use App\Models\Promotion;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        if(!(bool) Auth::user()->is_admin){
            return redirect(asset('/'));
        }
        $productsCount = Product::all()->count();
        $categoriesCount = Category::all()->count();
        $brandsCount = Brand::all()->count();
        $newsCount = News::all()->count();
        $promotionsCount = Promotion::all()->count();
        $ordersCount = Order::all()->count();
        $unservedOrdersCount = Order::where('served', '!=', 1)->orWhereNull('served')->count();

        $recentServedOrders = Order::latest()->limit(5)->where('served', 1)->get();
        $recentUnservedOrders = Order::latest()->limit(5)->where('served', '!=', 1)->orWhereNull('served')->get();
//        $recentNews = News::latest()->limit(5)->get();
//        $bannersCount = Banners::all()->count();
//        $clientsCount = Client::all()->count();
        $counts = [
            [
                'title' => 'Количесто Продуктов',
                'icon' => 'fas fa-file-image',
                'link' => 'admin/products',
                'data' => $productsCount
            ],
            [
                'title' => 'Количесто Категорий',
                'icon' => 'fas fa-folder-open',
                'link' => 'admin/categories',
                'data' => $categoriesCount
            ],
            [
                'title' => 'Количесто Брендов',
                'icon' => 'fas fa-copyright',
                'link' => 'admin/brands',
                'data' => $brandsCount
            ],
            [
                'title' => 'Количесто Новостей',
                'icon' => 'fas fa-newspaper',
                'link' => 'admin/news',
                'data' => $newsCount
            ],
            [
                'title' => 'Количесто Акций',
                'icon' => 'fas fa-tags',
                'link' => 'admin/promotions',
                'data' => $promotionsCount
            ],
            [
                'title' => 'Количесто Заказов',
                'icon' => 'fas fa-cart-arrow-down',
                'link' => 'admin/order',
                'data' => $ordersCount
            ],
            [
                'title' => 'Количесто Не выполненных Заказов',
                'icon' => 'fas fa-clipboard-list',
                'link' => 'admin/order',
                'data' => $unservedOrdersCount
            ],
        ];
        return view('admin.home', [
            'counts' => $counts,
            'recentServedOrders' => $recentServedOrders,
            'recentUnservedOrders' => $recentUnservedOrders,
        ]);
    }
}
