<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {
        $data = News::translated()->paginate(env('PAGINATION_SIZE'));
        foreach ($data as $url) $url->image = asset($url->image);
        return view('admin.news.main', ['data' => $data]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $n = new News();
        $langs = \App\Helpers\SysConfig::AllLanguage();
        $translations = [];
        foreach ($langs as $lang) {
            $translations[$lang->code] = [
                'title' => $request->get($lang->code)['title'],
                'content' => $request->get($lang->code)['content']
            ];
        }
        $n->fill($translations);
        $n->save();
        $n->refresh();
        if($request->image_news){
            $imageName = Str::random(40).".".$request->file("image_news")->extension();
            $path = 'news/'.$n->id;
            $fullpath = 'data/news/'.$n->id . '/' . $imageName;
            $file =   Storage::disk('uploads')->files($path);
            Storage::disk('uploads')->delete($file);
            $request->file("image_news")->storeAs($path, $imageName, 'uploads');
            $n->image = $fullpath;
            $n->save();
        }
        return Redirect::to(route('admin.news.index'))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(int $id)
    {
        $data = News::translated()->find($id);
        $langs = \App\Helpers\SysConfig::AllLanguage();
        $translations = [];
        foreach ($langs as $lang) {
            $translations[$lang->code] = [
                'title' => $data->translate($lang->code)->title,
                'content' => $data->translate($lang->code)->content,
            ];
        }
        $data->fill($translations);
        return view('admin.news.edit', ["data" => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $n = News::find($id);
        $langs = \App\Helpers\SysConfig::AllLanguage();
        $translations = [];
        foreach ($langs as $lang) {
            $translations[$lang->code] = [
                'title' => $request->get($lang->code)['title'],
                'content' => $request->get($lang->code)['content']
            ];
        }
        $n->fill($translations);
        if($request->image_news){
            $imageName = Str::random(40).".".$request->file("image_news")->extension();
            $path = 'news/'.$id;
            $fullpath = 'data/news/'.$id . '/' . $imageName;
            $file =   Storage::disk('uploads')->files($path);
            Storage::disk('uploads')->delete($file);
            $request->file("image_news")->storeAs($path, $imageName, 'uploads');
            $n->image = $fullpath;
        }
        $n->save();
        return Redirect::to(route('admin.news.index'))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        $news = News::find($id);
        $folder_path = public_path('data/news/'.$news->id);
        if (file_exists($folder_path)) {
            array_map('unlink', glob("$folder_path/*.*"));
            rmdir($folder_path);
        }
        $news->deleteTranslations();
        $news->delete();
        return Redirect::to(route('admin.news.index'))->with('message', 'Удаление прошло успешно');
    }
}
