<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Order::paginate(env('PAGINATION_SIZE'));
        return view('admin.order.main', ["data" => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Product::all();
        return view('admin.order.create', ["data" => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            "product_id" => "required"
        ]);
        if($validate->fails()){
            return Redirect::to(route('admin.order.create'))
                ->withErrors($validate);
        }

        $input = $request->all();
        /** @var Order $order */
        $order = Order::create($input);

        return Redirect::to(route('admin.order.index'))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Order::find($id);
        return view('admin.order.edit', ["data" => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            "served" => "required"
        ]);
        if($validate->fails()){
            return Redirect::back()
                ->withErrors($validate);
        }

        $served = (count($request->post('served'))===2)?1:0;

        $order = Order::find($id);
        $order->served = (bool) $served;
        $order->saveOrFail();

        return Redirect::to(route('admin.order.index'))->with('message', 'Редактирование прошло успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        Order::destroy($order->id);
        return Redirect::to(route('admin.order.index'))->with('message', 'Удаление прошло успешно');
    }
}
