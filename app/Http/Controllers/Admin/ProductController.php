<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use Faker\Core\File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\Input;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = Product::translated()->paginate(env('PAGINATION_SIZE'));
        return view('admin.product.main', ["data" => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $imageSections = ['on_main', 'on_description', 'on_requirement', 'on_version', 'on_buy_description'];
        return view('admin.product.create', ['imageSections' => $imageSections]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $product = new Product();
        $langs = \App\Helpers\SysConfig::AllLanguage();
        $translations = [];
        foreach ($langs as $lang) {
            $translations[$lang->code] = [
                'name' => $request->get($lang->code)['name'],
                'meta' => $request->get($lang->code)['meta'],
                'description' => $request->get($lang->code)['description'],
                'requirement' => $request->get($lang->code)['requirement'],
                'version' => $request->get($lang->code)['version'],
                'buy_description' => $request->get($lang->code)['buy_description'],
            ];
        }
        $product->fill($translations);
        $product->sku = $request->get('sku');
        $product->cost = $request->get('cost');
        $product->length = $request->get('length');
        $product->width = $request->get('width');
        $product->height = $request->get('height');
        $product->length_id = $request->get('length_id');
        $product->weight = $request->get('weight');
        $product->weight_id = $request->get('weight_id');
        $product->save();
        $product->refresh();
        if (!empty($request->get('category'))) {
            $product->productCategories()->createMany($request->get('category'));
        }
        if (!empty($request->get('brands'))) {
            $product->productBrands()->createMany($request->get('brands'));
        }
        if (!empty($request->get('attribute'))) {
            $product->productAttributes()->createMany($request->get('attribute'));
        }
        $imageSections = ['on_main', 'on_description', 'on_requirement', 'on_version', 'on_buy_description'];
        foreach ($imageSections as $section) {
            if ($request->file($section)) {
                $extension = $request->file($section)->extension();
                $imageName = Str::random(40).'_'.$section;
                $path = 'products/'.$product->id;
                $image = ProductImage::create([
                    'product_id' => $product->id,
                    $section => 1,
                    "image" => 'data/'.$path.'/'.$imageName.'.'.$extension
                ]);
                $request->file($section)->storeAs($path, str_replace('data/'.$path.'/', '', $image->image), 'uploads');
            }
        }
        $message = $product ? 'Сохранение прошло успешно' : 'Не удалось сохранить';
        return Redirect::to(route('admin.products.index'))->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Product::translated()->find($id);
        $langs = \App\Helpers\SysConfig::AllLanguage();
        $translations = [];
        foreach ($langs as $lang) {
            $translations[$lang->code] = [
                'name' => $data->translate($lang->code)->name,
                'meta' => $data->translate($lang->code)->meta,
                'description' => ltrim(strip_tags($data->translate($lang->code)->description)),
                'requirement' => ltrim(strip_tags($data->translate($lang->code)->requirement)),
                'version' => ltrim(strip_tags($data->translate($lang->code)->version)),
                'buy_description' => ltrim(strip_tags($data->translate($lang->code)->buy_description)),
            ];
        }
        $data->fill($translations);
        $imageSections = ['on_main', 'on_description', 'on_requirement', 'on_version', 'on_buy_description'];
        return view('admin.product.edit', ["data" => $data, 'imageSections' => $imageSections]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $langs = \App\Helpers\SysConfig::AllLanguage();
        $translations = [];
        foreach ($langs as $lang) {
            $translations[$lang->code] = [
                'name' => $request->get($lang->code)['name'],
                'meta' => $request->get($lang->code)['meta'],
                'description' => $request->get($lang->code)['description'],
                'requirement' => $request->get($lang->code)['requirement'],
                'version' => $request->get($lang->code)['version'],
                'buy_description' => $request->get($lang->code)['buy_description'],
            ];
        }
        //dd($translations);
        $product->fill($translations);
        $product->sku = $request->get('sku');
        $product->cost = $request->get('cost');
        $product->length = $request->get('length');
        $product->width = $request->get('width');
        $product->height = $request->get('height');
        $product->length_id = $request->get('length_id');
        $product->weight = $request->get('weight');
        $product->weight_id = $request->get('weight_id');
        $product->save();
        $product->productCategories()->delete();
        if (!empty($request->get('category'))) {
            $product->productCategories()->createMany($request->get('category'));
        }
        $product->productBrands()->delete();
        if (!empty($request->get('brands'))) {
            $product->productBrands()->createMany($request->get('brands'));
        }
        $product->productAttributes()->delete();
        if (!empty($request->get('attribute'))) {
            $product->productAttributes()->createMany($request->get('attribute'));
        }
        $imageSections = ['on_main', 'on_description', 'on_requirement', 'on_version', 'on_buy_description'];
        foreach ($imageSections as $section) {
            if ($request->file($section)) {
                $productImg = ProductImage::where(['product_id' => $id, $section => 1])->first();
                if ($productImg && file_exists($productImg->image)) {
                    unlink($productImg->image);
                }
                $extension = $request->file($section)->extension();
                $imageName = Str::random(40).'_'.$section;
                $path = 'products/'.$product->id;
                $image = ProductImage::updateOrCreate([
                    'product_id' => $id,
                    $section => 1
                ],[
                    'product_id' => $id,
                    $section => 1,
                    "image" => 'data/'.$path.'/'.$imageName.'.'.$extension
                ]);
                $request->file($section)->storeAs($path, str_replace('data/'.$path.'/', '', $image->image), 'uploads');
            }
        }
        return Redirect::to(route('admin.products.index'))->with('message', 'Редактирование прошло успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $folder_path = public_path('data/products/'.$product->id);
        if (file_exists($folder_path)) {
            array_map('unlink', glob("$folder_path/*.*"));
            rmdir($folder_path);
        }
        $product->delete();
        return Redirect::to(route('admin.products.index'))->with('message', 'Удаление прошло успешно');
    }
}
