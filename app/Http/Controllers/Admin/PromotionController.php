<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Promotion;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = Promotion::translated()->paginate(env('PAGINATION_SIZE'));
        foreach ($data as $url) $url->image = asset($url->image);
        return view('admin.promotions.main', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.promotions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $p = new Promotion();
        $langs = \App\Helpers\SysConfig::AllLanguage();
        $translations = [];
        foreach ($langs as $lang) {
            $translations[$lang->code] = [
                'title' => $request->get($lang->code)['title'],
                'content' => $request->get($lang->code)['content'],
            ];
        }
        $p->fill($translations);
        $p->save();
        $p->refresh();
        if($request->image_promot){
            $imageName = Str::random(40).".".$request->file("image_promot")->extension();
            $path = 'promotions/'.$p->id;
            $fullpath = 'data/promotions/'.$p->id . '/' . $imageName;
            $file =   Storage::disk('uploads')->files($path);
            Storage::disk('uploads')->delete($file);
            $request->file("image_promot")->storeAs($path, $imageName, 'uploads');
            $p->image = $fullpath;
            $p->save();
        }
        return Redirect::to(route('admin.promotions.index'))->with('message', 'Сохранение прошло успешно');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(int $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(int $id)
    {
        $data = Promotion::translated()->find($id);
        $langs = \App\Helpers\SysConfig::AllLanguage();
        $translations = [];
        foreach ($langs as $lang) {
            $translations[$lang->code] = [
                'title' => $data->translate($lang->code)->title,
                'content' => $data->translate($lang->code)->content,
            ];
        }
        $data->fill($translations);
        return view('admin.promotions.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $p = Promotion::find($id);
        $langs = \App\Helpers\SysConfig::AllLanguage();
        $translations = [];
        foreach ($langs as $lang) {
            $translations[$lang->code] = [
                'title' => $request->get($lang->code)['title'],
                'content' => $request->get($lang->code)['content'],
            ];
        }
        $p->fill($translations);
        if($request->image_promot){
            $imageName = Str::random(40).".".$request->file("image_promot")->extension();
            $path = 'promotions/'.$id;
            $fullpath = 'data/promotions/'.$id . '/' . $imageName;
            $file =   Storage::disk('uploads')->files($path);
            Storage::disk('uploads')->delete($file);
            $request->file("image_promot")->storeAs($path, $imageName, 'uploads');
            $p->image = $fullpath;
        }
        $p->save();
        return Redirect::to(route('admin.promotions.index'))->with('message', 'Сохранение прошло успешно!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        $c = Promotion::find($id);
        $folder_path = public_path('data/promotions/'.$c->id);
        if (file_exists($folder_path)) {
            array_map('unlink', glob("$folder_path/*.*"));
            rmdir($folder_path);
        }
        $c->delete();
        return Redirect::to(route('admin.promotions.index'))->with('message', 'Удаление прошло успешно!');
    }
}
