<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ShowFooter;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    protected $config;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $footer = new ShowFooter();
        $this->config = $footer->config();
    }

    public function showLinkRequestForm()
    {
        foreach ($this->config->translation()->get() as $var) $addresses[] = $var->address;
        $footer=$this->config;
        return view('auth.passwords.email', compact('footer','addresses'));
    }
}
