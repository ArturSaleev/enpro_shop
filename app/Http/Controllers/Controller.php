<?php

namespace App\Http\Controllers;

use App\Helpers\SysConfig;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $lang = 'ru';
    public function __construct(Request $request)
    {
        SysConfig::setDefaultLanguage();
        if($request->server('HTTP_ACCEPT_LANGUAGE')) {
            $lng = $request->server('HTTP_ACCEPT_LANGUAGE');
            if (!in_array($lng, ["ru", "en"])) {
                $lng = 'ru';
            }
            App::setLocale($lng);
            $this->lang = $lng;
        }

    }
}
