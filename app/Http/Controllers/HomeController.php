<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Product;
use App\Models\Promotion;
use App\Models\Banners;
use App\Models\News;
use App\Models\About;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $config;

    /**
     * Create a new DonorsController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $footer = new ShowFooter();
        $this->config = $footer->config();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $products = Product::all();
        $promotions = Promotion::all();
        $news = News::all();
        $banner = Banners::where("active","=",1)->first();
        $clients = Client::inRandomOrder()->take(6)->get();
        $about = About::latest()->first();
        foreach ($this->config->translation()->get() as $var) $addresses[] = $var->address;
        return view(
            'welcome',
            [
                'products' => $products,
                'promotions' => $promotions,
                'news' => $news,
                'clients' => $clients,
                'banner' => $banner,
                'about' => $about,
                'footer' => $this->config,
                'addresses' => $addresses,
            ]
        );
    }
    /**
     * Show about page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function about(Request $request)
    {
        $products = Product::all();
        $promotions = Promotion::all();
        $news = News::all();
        $clients = Client::inRandomOrder()->take(6)->get();
        return view(
            'about',
            [
                'products' => $products,
                'promotions' => $promotions,
                'news' => $news,
                'clients' => $clients,
            ]
        );
    }
}
