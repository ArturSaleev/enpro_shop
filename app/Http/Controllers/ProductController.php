<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Client;
use App\Models\Locale;
use App\Models\News;
use App\Models\Product;
use App\Models\Promotion;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $config;

    /**
     * Create a new DonorsController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $footer = new ShowFooter();
        $this->config = $footer->config();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $categories = Category::all();
        $promotions = Promotion::all();
        $news = News::all();
        $clients = Client::inRandomOrder()->take(6)->get();
        session()->forget('filter');
        foreach ($this->config->translation()->get() as $var) $addresses[] = $var->address;
        return view('products',
            [
                'products' => $products,
                'categories' => $categories,
                'promotions' => $promotions,
                'news' => $news,
                'clients' => $clients,
                'footer' => $this->config,
                'addresses' => $addresses,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $promotions = Promotion::all();
        $news = News::all();
        foreach ($this->config->translation()->get() as $var) $addresses[] = $var->address;
        return view('product_main',[
            'product' => $product,
            'promotions' => $promotions,
            'news' => $news,
            'footer' => $this->config,
            'addresses' => $addresses,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    /**
     * sort by filter
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        $arrayFilter = [];
        $categories = Category::all();
        $promotions = Promotion::all();
        $news = News::all();
        $arrayFilter = session()->get("filter");
        if (!in_array($request->filter, ($arrayFilter?:[]))) {
            $arrayFilter[] = $request->filter;
            session()->put('filter', $arrayFilter);
        }
        $products = Product::whereHas('productCategories', function (Builder $query) use($arrayFilter) {
            $query->whereIn('category_id', $arrayFilter);
        })->get();
        foreach ($categories as $category)
            foreach ($arrayFilter as $active)
                if($category->id == $active) $category->setFilterActiveAttribute(true);

        foreach ($this->config->translation()->get() as $var) $addresses[] = $var->address;
        return view('products',
            [
                'products' => $products,
                'categories' => $categories,
                'filter' => $arrayFilter,
                'promotions' => $promotions,
                'news' => $news,
                'footer' => $this->config,
                'addresses' => $addresses,
            ]
        );
    }

    /**
     * search by SKU
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $products = Product::where('sku', 'LIKE', "%{$request->sku}%" )->get();
        $categories = Category::all();
        $promotions = Promotion::all();
        $news = News::all();
        session()->forget('filter');
        foreach ($this->config->translation()->get() as $var) $addresses[] = $var->address;
        return view('products',
            [
                'products' => $products,
                'categories' => $categories,
                'promotions' => $promotions,
                'news' => $news,
                'footer' => $this->config,
                'addresses' => $addresses,
            ]
        );
    }
}
