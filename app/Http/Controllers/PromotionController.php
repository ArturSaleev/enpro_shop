<?php

namespace App\Http\Controllers;

use App\Models\Promotion;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    protected $config;

    /**
     * Create a new DonorsController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $footer = new ShowFooter();
        $this->config = $footer->config();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotions = Promotion::all();
        foreach ($this->config->translation()->get() as $var) $addresses[] = $var->address;
        return view('stocks',
            [
                'promotions' => $promotions,
                'footer' => $this->config,
                'addresses' => $addresses,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $promotions = Promotion::all();
        $promotion = Promotion::find($id);
        foreach ($this->config->translation()->get() as $var) $addresses[] = $var->address;
        return view('stock_main',[
            'promotion' => $promotion,
            'promotions' => $promotions,
            'footer' => $this->config,
            'addresses' => $addresses,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Promotion  $promotions
     * @return \Illuminate\Http\Response
     */
    public function edit(Promotion $promotions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Promotion  $promotions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promotion $promotions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Promotion  $promotions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promotion $promotions)
    {
        //
    }
}
