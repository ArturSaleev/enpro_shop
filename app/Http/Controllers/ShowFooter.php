<?php

namespace App\Http\Controllers;

use App\Models\Config;

class ShowFooter extends Controller
{

    public function __construct()
    {
        //
    }
    /**
     * Handle the incoming request.
     *
     * @return \App\Models\Config
     */
    public function config()
    {
        return Config::latest()->first();
    }
}
