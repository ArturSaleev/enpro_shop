<?php

namespace App\Http\Middleware;

use App\Models\Locale;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(session()->get("locale")){
            App::setLocale(session()->get("locale"));
        }else{
            $locale = Locale::where('on_default', '=', 1)->first()->code;
            session()->put('locale', $locale);
            App::setLocale($locale);
        }
        return $next($request);
    }
}
