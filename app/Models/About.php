<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $image
 * @property AboutTranslation[] $aboutTranslations
 */
class About extends Model implements TranslatableContract
{

    public $table = "about";

    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = ['content_main', 'content_page','content_main_front', 'content_page_front'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['image'];

    /**
     * @return HasMany
     */
    public function aboutTranslations()
    {
        return $this->hasMany('App\Models\AboutTranslation');
    }
}
