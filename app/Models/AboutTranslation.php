<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $about_id
 * @property string $locale
 * @property string $content_main
 * @property string $content_page
 * @property About $about
 */
class AboutTranslation extends Model
{
    protected $keyType = 'integer';
    public $timestamps = false;
    protected $fillable = ['about_id', 'locale', 'content_main', 'content_page','content_main_front', 'content_page_front'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function about()
    {
        return $this->belongsTo('App\Models\About');
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getContentMainFrontAttribute()
    {
        $front = "<div class=\"py-3\"> <div class=\"container-fluid\" style=\" background-image: url('@productimage@'); background-repeat: no-repeat; background-size: 49%; background-color: #EBF7FF; background-position: left; min-height: 30vw; \"> <div class=\"swiper-slide-caption section-md container\"> <div class=\"py-7 row\"> <div class=\"col-lg-6\"> </div> <div class=\"col-lg-6\"> <h3>".$this->attributes['content_main']."<br><br> <a href=\"".asset('about')."\" class=\"btn btn-primary\">". __('all.details') ."</a> </div> </div> </div> </div> </div>";
        $front = str_replace("@productimage@",asset(About::where("id","=",$this->attributes['about_id'])->first()->image),$front);
        return $front;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getContentPageFrontAttribute()
    {
        $front = "<div class=\"container\"> <div class=\"row\"> <div class=\"col-lg-12\"> <div class=\"float-start\" style=\"width: 48%\"> <img src=\"@productimage@\" width=\"90%\" class=\"img-fluid m-2\"> </div> <br> ".$this->attributes['content_page']." </div> </div> </div>";
        $front = str_replace("@productimage@",asset(About::where("id","=",$this->attributes['about_id'])->first()->image),$front);
        return $front;
    }
}
