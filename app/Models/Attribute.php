<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property integer $id
 * @property boolean $on_view
 * @property AttributeParam[] $attributeParams
 * @property AttributeTranslation[] $attributeTranslations
 */
class Attribute extends Model implements TranslatableContract
{
    use Translatable;
    public $translatedAttributes = ['name', 'content'];
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['on_view'];

    /**
     * @return HasMany
     */
    public function attributeParams()
    {
        return $this->hasMany('App\Models\AttributeParam');
    }

    /**
     * @return HasMany
     */
    public function attributeTranslations()
    {
        return $this->hasMany('App\Models\AttributeTranslation');
    }
}
