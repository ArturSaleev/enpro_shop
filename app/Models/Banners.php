<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $image
 * @property BannersTranslation[] $bannersTranslations
 */
class Banners extends Model implements TranslatableContract
{
    public $table = "banners";

    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = ['title', 'content', 'content_front'];

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['image','active'];

    /**
     * @return HasMany
     */
    public function bannersTranslations()
    {
        return $this->hasMany('App\Models\BannersTranslation');
    }
}
