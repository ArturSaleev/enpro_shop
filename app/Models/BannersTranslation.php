<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $news_id
 * @property string $locale
 * @property string $title
 * @property string $content
 * @property Banners $banners
 */
class BannersTranslation extends Model
{
    protected $keyType = 'integer';
    public $timestamps = false;
    protected $fillable = ['banners_id', 'locale', 'title', 'content', 'content_front'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function banners()
    {
        return $this->belongsTo('App\Models\Banners');
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getContentAttribute()
    {
        $this->attributes['content'] = str_replace("@producttitle@",$this->attributes['title'],$this->attributes['content']);
        $this->attributes['content'] = str_replace("@productimage@",asset(Banners::where("id","=",$this->attributes['banners_id'])->first()->image),$this->attributes['content']);
        return $this->attributes['content'];
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getContentFrontAttribute()
    {
        $front = "<div class=\"container-fluid\" style=\" background-image: url('@productimage@'); background-repeat: no-repeat; background-size: 40%; background-color: #EBF7FF; background-position: right; min-height: 30vw; \"> <div class=\"swiper-slide-caption section-md container\"> <div class=\"py-7 row\"> <div class=\"col-lg-6\"> <h3>@producttitle@</h3> <br> ".$this->attributes['content']." <br><br> </div> <div class=\"col-lg-6\"> </div> </div> </div> </div>";
        $front = str_replace("@producttitle@",$this->attributes['title'],$front);
        $front = str_replace("@productimage@",asset(Banners::where("id","=",$this->attributes['banners_id'])->first()->image),$front);
        return $front;
    }
}
