<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $name
 * @property string $image
 * @property BrandTranslation[] $brandTranslations
 */
class Brand extends Model implements TranslatableContract
{
    use Translatable;
    use SoftDeletes;
    public $translatedAttributes = ['content'];
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'image'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function brandTranslations()
    {
        return $this->hasMany('App\Models\BrandTranslation');
    }
}
