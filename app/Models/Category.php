<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $parent_id
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 * @property CategoryTranslation[] $categoryTranslations
 */
class Category extends Model implements TranslatableContract
{
    use Translatable;
    use SoftDeletes;
    public $translatedAttributes = ['name', 'content'];
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['parent_id', 'image', 'created_at', 'updated_at'];

    /**
     * @return HasMany
     */
    public function categoryTranslations()
    {
        return $this->hasMany('App\Models\CategoryTranslation');
    }

    /**
     * Get active filter.
     *
     * @return string
     */
    public function getFilterActiveAttribute()
    {
        return $this->active;
    }

    /**
     * Set active filter.
     *
     * @return string
     */
    public function setFilterActiveAttribute($value)
    {
        $this->attributes['active'] = $value;
    }

}
