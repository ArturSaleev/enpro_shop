<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $name
 * @property ClientTranslation[] $clientsTranslations
 */
class Client extends Model implements TranslatableContract
{
    use Translatable;
    use SoftDeletes;
    public $translatedAttributes = ['content'];

    public $table = "clients";

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'image'];

    /**
     * @return HasMany
     */
    public function clientsTranslations()
    {
        return $this->hasMany('App\Models\ClientTranslation', 'clients_id');
    }
}
