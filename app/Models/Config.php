<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property integer $id
 * @property string $logo
 * @property string $icon
 * @property string $email
 * @property ConfigAddress[] $configAddresses
 */
class Config extends Model implements TranslatableContract
{
    use Translatable;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $translatedAttributes = ['address'];

    protected $table = 'config';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['logo', 'icon', 'email', 'phone'];

    /**
     * @return HasOne
     */
    public function configAddresses()
    {
        return $this->hasMany('App\Models\ConfigAddress');
    }
}
