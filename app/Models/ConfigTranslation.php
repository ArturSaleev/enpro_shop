<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer $id
 * @property integer $config_id
 * @property string $locale
 * @property string $address
 * @property Config $config
 */
class ConfigTranslation extends Model
{

    public $timestamps = false;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['config_id', 'locale', 'address'];

    /**
     * @return BelongsTo
     */
    public function config()
    {
        return $this->belongsTo('App\Models\Config');
    }
}
