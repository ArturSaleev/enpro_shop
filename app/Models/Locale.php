<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $code
 * @property string $lang_code
 * @property string $name
 * @property string $display_name
 * @property string $image
 * @property boolean $on_default
 */
class Locale extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['created_at', 'updated_at', 'code', 'lang_code', 'name', 'display_name', 'image', 'on_default'];

    /**
     * @return ?Locale
     */
    public static function getLocale()
    {
        return Locale::where('code', '=', getLocaleDB());
    }
}
