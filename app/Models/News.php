<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property boolean $status
 * @property int $sort
 * @property string $image
 * @property NewsTranslation[] $newsTranslations
 */
class News extends Model implements TranslatableContract
{
    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = ['title', 'content', 'content_front'];

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['status', 'sort', 'image'];

    /**
     * @return HasMany
     */
    public function newsTranslations()
    {
        return $this->hasMany('App\Models\NewsTranslation');
    }
}
