<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $news_id
 * @property string $locale
 * @property string $title
 * @property string $content
 * @property News $news
 */
class NewsTranslation extends Model
{
    protected $keyType = 'integer';
    public $timestamps = false;
    protected $fillable = ['news_id', 'locale', 'title', 'content', 'content_front'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function news()
    {
        return $this->belongsTo('App\Models\News');
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getContentFrontAttribute()
    {
        $front = "<div class=\"container\"> <div class=\"row\"> <div class=\"col-lg-12\"> <div class=\"float-start\" style=\"width: 48%\"> <img src=\"@productimage@\" width=\"90%\" class=\"img-fluid m-2\"> </div> <p><h4>@producttitle@</h4></p> ".$this->attributes['content']." </div> </div> </div>";
        $front = str_replace("@producttitle@",$this->attributes['title'],$front);
        $front = str_replace("@productimage@",asset(News::where("id","=",$this->attributes['news_id'])->first()->image),$front);
        return $front;
    }
}
