<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property boolean $state
 * @property string $sku
 * @property float $cost
 * @property integer $length_id
 * @property float $length
 * @property float $width
 * @property float $height
 * @property integer $weight_id
 * @property float $weight
 * @property int $minimal
 * @property integer $image_main
 * @property ProductAttribute[] $productAttributes
 * @property ProductBrand[] $productBrands
 * @property ProductCategory[] $productCategories
 * @property ProductFile[] $productFiles
 * @property ProductImage[] $productImages
 * @property ProductRecomend[] $productIdRecomends
 * @property ProductRecomend[] $productRecomends
 * @property ProductTranslation[] $productTranslations
 */
class Product extends Model implements TranslatableContract
{
    use Translatable;
    use HasFactory;
    use SoftDeletes;
    public $translatedAttributes = ['name', 'meta', 'description', 'requirement', 'version', 'buy_description', 'description_front', 'requirement_front', 'version_front', 'buy_description_front'];
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['state', 'sku', 'cost', 'length_id', 'length', 'width', 'height', 'weight_id', 'weight', 'minimal', 'image_main'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productAttributes()
    {
        return $this->hasMany('App\Models\ProductAttribute');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productBrands()
    {
        return $this->hasMany('App\Models\ProductBrand');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productCategories()
    {
        return $this->hasMany('App\Models\ProductCategory');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productFiles()
    {
        return $this->hasMany('App\Models\ProductFile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productImages()
    {
        return $this->hasMany('App\Models\ProductImage');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productIdRecomends()
    {
        return $this->hasMany('App\Models\ProductRecomend', 'product_id_recomend');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productRecomends()
    {
        return $this->hasMany('App\Models\ProductRecomend');
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getImageMainAttribute()
    {
        $imagemain = ProductImage::where("product_id","=",$this->attributes['id'])->where('on_main', '=', 1)->first();
        return $imagemain?$imagemain->image:"";
    }

}
