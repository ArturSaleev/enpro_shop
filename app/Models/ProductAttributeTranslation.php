<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $product_attribute_id
 * @property string $locale
 * @property string $value
 * @property ProductAttribute $productAttribute
 */
class ProductAttributeTranslation extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['product_attribute_id', 'locale', 'value'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productAttribute()
    {
        return $this->belongsTo('App\Models\ProductAttribute');
    }
}
