<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $product_id
 * @property string $filename
 * @property Product $product
 * @property ProductFileTranslation[] $productFileTranslations
 */
class ProductFile extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['content'];

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['product_id', 'filename'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productFileTranslations()
    {
        return $this->hasMany('App\Models\ProductFileTranslation');
    }
}
