<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $product_file_id
 * @property string $locale
 * @property string $content
 * @property ProductFile $productFile
 */
class ProductFileTranslation extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['product_file_id', 'locale', 'content'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productFile()
    {
        return $this->belongsTo('App\Models\ProductFile');
    }
}
