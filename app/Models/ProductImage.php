<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $product_id
 * @property string $image
 * @property boolean $on_main
 * @property boolean $on_description
 * @property boolean $on_requirement
 * @property boolean $on_version
 * @property boolean $on_buy_description
 * @property Product $product
 */
class ProductImage extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['product_id', 'image', 'on_main', 'on_description', 'on_requirement', 'on_version', 'on_buy_description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
