<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $product_id
 * @property string $locale
 * @property string $name
 * @property string $meta
 * @property string $description
 * @property string $requirement
 * @property string $version
 * @property string $buy_description
 * @property Product $product
 */
class ProductTranslation extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['product_id', 'locale', 'name', 'meta', 'description', 'requirement', 'version', 'buy_description', 'description_front', 'requirement_front', 'version_front', 'buy_description_front'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescriptionFrontAttribute()
    {
        $front = "<div class=\"container\"> <div class=\"row\"> <div class=\"col-lg-12\"> <div class=\"float-end\" style=\"width: 48%\"> <img src=\"@productimage@\" width=\"90%\" class=\"img-fluid m-2\"> </div> ".$this->attributes['description']." </div> </div> </div>";
        $front = str_replace("@productimage@",asset(ProductImage::select('image')->where("product_id","=",$this->attributes['product_id'])->where('on_description', '=', 1)->first()->image),$front);
        return $front;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getRequirementFrontAttribute()
    {
        $front = "<div class=\"container\"> <div class=\"row\"> <div class=\"col-lg-12\"> <div class=\"float-end\" style=\"width: 48%\"> <img src=\"@productimage@\" width=\"90%\" class=\"img-fluid m-2\"> </div> ".$this->attributes['description']." </div> </div> </div>";
        $front = str_replace("@productimage@",asset(ProductImage::select('image')->where("product_id","=",$this->attributes['product_id'])->where('on_requirement', '=', 1)->first()->image),$front);
        return $front;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getVersionFrontAttribute()
    {
        $front = "<div class=\"container\"> <div class=\"row\"> <div class=\"col-lg-12\"> <div class=\"float-end\" style=\"width: 48%\"> <img src=\"@productimage@\" width=\"90%\" class=\"img-fluid m-2\"> </div> ".$this->attributes['description']." </div> </div> </div>";
        $front = str_replace("@productimage@",asset(ProductImage::select('image')->where("product_id","=",$this->attributes['product_id'])->where('on_version', '=', 1)->first()->image),$front);
        return $front;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getBuyDescriptionFrontAttribute()
    {
        $front = "<div class=\"container\"> <div class=\"row\"> <div class=\"col-lg-12\"> <div class=\"float-end\" style=\"width: 48%\"> <img src=\"@productimage@\" width=\"90%\" class=\"img-fluid m-2\"> </div> ".$this->attributes['description']." </div> </div> </div>";
        $front = str_replace("@productimage@",asset(ProductImage::select('image')->where("product_id","=",$this->attributes['product_id'])->where('on_buy_description', '=', 1)->first()->image),$front);
        return $front;
    }
}
