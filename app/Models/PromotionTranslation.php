<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $promotion_id
 * @property string $locale
 * @property string $title
 * @property string $content
 * @property Promotion $promotion
 */
class PromotionTranslation extends Model
{
    protected $keyType = 'integer';
    public $timestamps = false;
    protected $fillable = ['promotion_id', 'locale', 'title', 'content', 'content_front'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function promotion()
    {
        return $this->belongsTo('App\Models\Promotion');
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getContentFrontAttribute()
    {
        $front = "<div class=\"container\"> <div class=\"row\"> <div class=\"col-lg-12\"> <div class=\"float-start\" style=\"width: 48%\"> <img src=\"@productimage@\" width=\"90%\" class=\"img-fluid m-2\"> </div> <p><h4>".__("all.promotion")."!</h4></p> ".$this->attributes['content']." </div> </div> </div>";
        $front = str_replace("@productimage@",asset(Promotion::where("id","=",$this->attributes['promotion_id'])->first()->image),$front);
        return $front;
    }
}
