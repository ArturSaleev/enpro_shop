<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property RefLengthTranslation[] $refLengthTranslations
 */
class RefLength extends Model implements TranslatableContract
{
    use Translatable;
    public $translatedAttributes = ['name', 'abbreviate'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ref_length';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function refLengthTranslations()
    {
        return $this->hasMany('App\Models\RefLengthTranslation');
    }
}
