<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property RefWeightTranslation[] $refWeightTranslations
 */
class RefWeight extends Model implements TranslatableContract
{
    use Translatable;
    public $translatedAttributes = ['name', 'abbreviate'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ref_weight';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function refWeightTranslations()
    {
        return $this->hasMany('App\Models\RefWeightTranslation');
    }
}
