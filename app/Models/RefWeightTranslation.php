<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $ref_weight_id
 * @property string $locale
 * @property string $name
 * @property string $abbreviate
 * @property RefWeight $refWeight
 */
class RefWeightTranslation extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['ref_weight_id', 'locale', 'name', 'abbreviate'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function refWeight()
    {
        return $this->belongsTo('App\Models\RefWeight');
    }
}
