<?php

use App\Models\Locale;

function getLocaleDB()
{
    return session('locale')?Locale::where('code', '=', session('locale'))->first()->id:Locale::where('code', '=', config('app.fallback_locale'))->first()->id;
}
