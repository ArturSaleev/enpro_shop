<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'sku' => $this->faker->name(),
            'cost' => random_int(100, 999),
            'height' => random_int(1, 99),
            'length' => random_int(1, 99),
            'minimal' => random_int(1, 99),
            'weight' => random_int(1, 99),
            'width' => random_int(100, 999)
        ];
    }
}
