<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToNewsTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('news_translations', function(Blueprint $table)
		{
            $table->unique(['news_id', 'locale']);
			$table->foreign('news_id', 'FK_news_translations_news_id')->references('id')->on('news')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('news_translations', function(Blueprint $table)
		{
			$table->dropForeign('FK_news_translations_news_id');
		});
	}

}
