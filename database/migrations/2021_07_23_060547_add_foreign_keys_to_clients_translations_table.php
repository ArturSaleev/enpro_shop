<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToClientsTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('client_translations', function(Blueprint $table)
		{
            $table->unique(['client_id', 'locale']);
			$table->foreign('client_id', 'FK_clients_translations_clients_id')->references('id')->on('clients')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('client_translations', function(Blueprint $table)
		{
			$table->dropForeign('FK_client_translations_client_id');
		});
	}

}
