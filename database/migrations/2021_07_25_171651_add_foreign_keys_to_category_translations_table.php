<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToCategoryTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('category_translations', function(Blueprint $table)
		{
            $table->unique(['category_id', 'locale']);
			$table->foreign('category_id', 'FK_category_translations_categories_id')->references('id')->on('categories')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('category_translations', function(Blueprint $table)
		{
			$table->dropForeign('FK_category_translations_categories_id');
		});
	}

}
