<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToBrandTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('brand_translations', function(Blueprint $table)
		{
            $table->unique(['brand_id', 'locale']);
			$table->foreign('brand_id', 'FK_brand_translations_brands_id')->references('id')->on('brands')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('brand_translations', function(Blueprint $table)
		{
			$table->dropForeign('FK_brand_translations_brands_id');
		});
	}

}
