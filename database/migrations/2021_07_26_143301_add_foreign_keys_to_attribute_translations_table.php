<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToAttributeTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('attribute_translations', function(Blueprint $table)
		{
            $table->unique(['attribute_id', 'locale']);
			$table->foreign('attribute_id', 'FK_attribute_translations_attributes_id')->references('id')->on('attributes')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('attribute_translations', function(Blueprint $table)
		{
			$table->dropForeign('FK_attribute_translations_attributes_id');
		});
	}

}
