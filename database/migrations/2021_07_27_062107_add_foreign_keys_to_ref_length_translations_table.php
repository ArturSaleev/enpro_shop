<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToRefLengthTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ref_length_translations', function(Blueprint $table)
		{
            $table->unique(['ref_length_id', 'locale']);
			$table->foreign('ref_length_id', 'FK_ref_length_translations_ref_length_id')->references('id')->on('ref_length')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ref_length_translations', function(Blueprint $table)
		{
			$table->dropForeign('FK_ref_length_translations_ref_length_id');
		});
	}

}
