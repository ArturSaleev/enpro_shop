<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToRefWeightTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ref_weight_translations', function(Blueprint $table)
		{
            $table->unique(['ref_weight_id', 'locale']);
			$table->foreign('ref_weight_id', 'FK_ref_weight_translations_ref_weight_id')->references('id')->on('ref_weight')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ref_weight_translations', function(Blueprint $table)
		{
			$table->dropForeign('FK_ref_weight_translations_ref_weight_id');
		});
	}

}
