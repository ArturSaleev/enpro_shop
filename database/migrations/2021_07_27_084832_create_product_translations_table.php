<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_translations', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('product_id')->unsigned()->index('FK_product_translations_products_id');
			$table->string('locale');
			$table->string('name');
			$table->string('meta')->nullable();
			$table->text('description')->nullable()->comment('Описание');
			$table->text('requirement')->nullable()->comment('Минимальные требования');
			$table->text('version')->nullable()->comment('Версия');
			$table->text('buy_description')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_translations');
	}

}
