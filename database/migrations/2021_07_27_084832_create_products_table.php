<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->boolean('state')->default(1);
			$table->string('sku');
			$table->decimal('cost', 19)->default(0.00);
			$table->bigInteger('length_id')->unsigned()->nullable();
			$table->float('length', 10, 0)->default(0)->comment('Длина');
			$table->float('width', 10, 0)->default(0)->comment('Ширина');
			$table->float('height', 10, 0)->default(0)->comment('Высота');
			$table->bigInteger('weight_id')->unsigned()->default(0);
			$table->float('weight', 10, 0)->default(0)->comment('вес');
			$table->integer('minimal')->default(1);
			$table->timestamps();
            $table->softDeletes($column = 'deleted_at', $precision = 0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
