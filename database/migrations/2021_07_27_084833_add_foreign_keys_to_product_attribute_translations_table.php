<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToProductAttributeTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_attribute_translations', function(Blueprint $table)
		{
		    $table->unique(['product_attribute_id', 'locale'], "product_attr_trans_product_attr_id_locale_unique");
			$table->foreign('product_attribute_id', 'FK_product_attribute_translations_product_attributes_id')->references('id')->on('product_attributes')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_attribute_translations', function(Blueprint $table)
		{
			$table->dropForeign('FK_product_attribute_translations_product_attributes_id');
		});
	}

}
