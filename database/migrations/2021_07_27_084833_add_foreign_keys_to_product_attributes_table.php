<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToProductAttributesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_attributes', function(Blueprint $table)
		{
			$table->foreign('attribute_id', 'FK_product_attributes_attributes_id')->references('id')->on('attributes')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('product_id', 'FK_product_attributes_products_id')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_attributes', function(Blueprint $table)
		{
			$table->dropForeign('FK_product_attributes_attributes_id');
			$table->dropForeign('FK_product_attributes_products_id');
		});
	}

}
