<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToProductBrandsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_brands', function(Blueprint $table)
		{
			$table->foreign('brand_id', 'FK_product_brands_brands_id')->references('id')->on('brands')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('product_id', 'FK_product_brands_products_id')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_brands', function(Blueprint $table)
		{
			$table->dropForeign('FK_product_brands_brands_id');
			$table->dropForeign('FK_product_brands_products_id');
		});
	}

}
