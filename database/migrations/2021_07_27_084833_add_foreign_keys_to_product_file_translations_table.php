<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToProductFileTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_file_translations', function(Blueprint $table)
		{
            $table->unique(['product_file_id', 'locale']);
			$table->foreign('product_file_id', 'FK_product_file_translations_product_files_id')->references('id')->on('product_files')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_file_translations', function(Blueprint $table)
		{
			$table->dropForeign('FK_product_file_translations_product_files_id');
		});
	}

}
