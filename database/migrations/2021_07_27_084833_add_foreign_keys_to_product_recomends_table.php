<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToProductRecomendsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_recomends', function(Blueprint $table)
		{
			$table->foreign('product_id_recomend', 'FK_pr_products_id_rec')->references('id')->on('products')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('product_id', 'FK_product_recomends_products_id')->references('id')->on('products')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_recomends', function(Blueprint $table)
		{
			$table->dropForeign('FK_pr_products_id_rec');
			$table->dropForeign('FK_product_recomends_products_id');
		});
	}

}
