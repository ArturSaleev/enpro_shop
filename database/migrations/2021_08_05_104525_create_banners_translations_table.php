<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners_translations', function (Blueprint $table) {
            $table->bigInteger('id', true)->unsigned();
            $table->bigInteger('banners_id')->unsigned()->index('FK_banners_translations_banners_id');
            $table->string('locale')->index();
            $table->string('title');
            $table->text('content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners_translations');
    }
}
