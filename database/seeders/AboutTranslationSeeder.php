<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AboutTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('about_translations')->insert([
            'about_id' => 1,
            'locale' => 'ru',
            'content_main' => '<h3>О компании</h3><br>Задача организации, в особенности же консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Товарищи! реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании позиций, занимаемых участниками в отношении поставленных задач.<br><br>',
            'content_page' => '<h3>О компании</h3><br>Задача организации, в особенности же консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Товарищи! реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании позиций, занимаемых участниками в отношении поставленных задач.<br><br><h3>Реквизиты</h3><br>Задача организации, в особенности же консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Товарищи! реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании позиций, занимаемых участниками в отношении поставленных задач.',
        ]);
        DB::table('about_translations')->insert([
            'about_id' => 1,
            'locale' => 'en',
            'content_main' => '<h3>About company</h3><br>The task of the organization, especially the consultation of a broad range of (specialists) ensures the participation of a broad range of (specialists) in shaping the forms of development. Comrades! The realization of the planned tasks ensures the participation of a wide range (of specialists) in the formation of the positions taken by the participants with regard to the tasks set.<br><br>',
            'content_page' => '<h3>About company</h3><br>The task of the organization, especially the consultation of a broad range of (specialists) ensures the participation of a broad range of (specialists) in shaping the forms of development. Comrades! The realization of the planned tasks ensures the participation of a wide range (of specialists) in the formation of the positions taken by the participants with regard to the tasks set.<br><br><h3>Credentials</h3><br>The task of the organization, especially the consultation of a broad range of (specialists) ensures the participation of a broad range of (specialists) in shaping the forms of development. Comrades! The realization of the planned tasks ensures the participation of a wide range (of specialists) in the formation of the positions taken by the participants with regard to the tasks set.',
        ]);
    }
}
