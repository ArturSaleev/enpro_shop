<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BannersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('banners')->truncate();
        DB::table('banners')->insert([
            'image' => 'data/banner/1.png',
            'active' => true,
        ]);
    }
}
