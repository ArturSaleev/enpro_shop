<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BannersTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('banners_translations')->truncate();
        DB::table('banners_translations')->insert([
            'banners_id' => 1,
            'locale' => 'ru',
            'title' => 'Как начать?',
            'content' => "Платформа Windows для IoT доступна в двух изданиях, каждое из которых оптимизировано под конкретные сценарии. Оба издания обладают безопасностью, управляемостью, возможностями поддержки и подключением к облачным службам, которых вы ожидаете от Windows. Создать устройство не сложнее, чем создать приложение."
        ]);
        DB::table('banners_translations')->insert([
            'banners_id' => 1,
            'locale' => 'en',
            'title' => 'How to get started?',
            'content' => "The Windows Platform for IoT is available in two editions, each optimized for specific scenarios. Both editions have the security, manageability, support capabilities, and connectivity to cloud services that you expect from Windows. Creating a device is as easy as creating an application."
        ]);
    }
}
