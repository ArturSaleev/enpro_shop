<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            'name' => 'Microsoft',
            'image' => 'data/clients/microsoft.png',
        ]);
        DB::table('brands')->insert([
            'name' => 'Kaspersky',
            'image' => 'data/clients/kaspersky.png',
        ]);
        DB::table('brands')->insert([
            'name' => 'Cisco',
            'image' => 'data/clients/cisco.png',
        ]);
    }
}
