<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrandTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brand_translations')->insert([
            'brand_id' => 1,
            'locale' => 'ru',
            'content' => 'Найдите продукты и услуги Microsoft для дома или бизнеса. Выбирайте Microsoft 365 для бизнеса, Xbox, Windows, Azure и многое другое.',
        ]);
        DB::table('brand_translations')->insert([
            'brand_id' => 1,
            'locale' => 'en',
            'content' => 'Find Microsoft products and services for your home or business. Choose from Microsoft 365 for business, Xbox, Windows, Azure and more.',
        ]);
        DB::table('brand_translations')->insert([
            'brand_id' => 2,
            'locale' => 'ru',
            'content' => 'Kaspersky Endpoint Security Cloud — корпоративное антивирусное решение для компаний малого и среднего бизнеса. Защищает рабочие станции, мобильные устройства.',
        ]);
        DB::table('brand_translations')->insert([
            'brand_id' => 2,
            'locale' => 'en',
            'content' => 'Kaspersky Endpoint Security Cloud is a corporate anti-virus solution for small and medium-sized businesses. It protects workstations, mobile devices.',
        ]);
        DB::table('brand_translations')->insert([
            'brand_id' => 3,
            'locale' => 'ru',
            'content' => 'Cisco Networking Innovation Forum. Присоединяйтесь, чтобы быть в курсе последних инноваций, трендов и решений.',
        ]);
        DB::table('brand_translations')->insert([
            'brand_id' => 3,
            'locale' => 'en',
            'content' => 'Cisco Networking Innovation Forum. Join to stay on top of the latest innovations, trends, and solutions.',
        ]);
    }
}
