<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('categories')->truncate();
        DB::table('categories')->insert([
            'image' => 'data/file/noimage.png',
        ]);
        DB::table('categories')->insert([
            'image' => 'data/file/noimage.png',
            'parent_id' => '1',
        ]);
        DB::table('categories')->insert([
            'image' => 'data/file/noimage.png',
            'parent_id' => '1',
        ]);
        DB::table('categories')->insert([
            'image' => 'data/file/noimage.png',
            'parent_id' => '1',
        ]);
        DB::table('categories')->insert([
            'image' => 'data/file/noimage.png',
            'parent_id' => '1',
        ]);
        DB::table('categories')->insert([
            'image' => 'data/file/noimage.png',
        ]);
        DB::table('categories')->insert([
            'image' => 'data/file/noimage.png',
            'parent_id' => '6',
        ]);
        DB::table('categories')->insert([
            'image' => 'data/file/noimage.png',
            'parent_id' => '6',
        ]);
        DB::table('categories')->insert([
            'image' => 'data/file/noimage.png',
            'parent_id' => '6',
        ]);
        DB::table('categories')->insert([
            'image' => 'data/file/noimage.png',
        ]);
        DB::table('categories')->insert([
            'image' => 'data/file/noimage.png',
            'parent_id' => '10',
        ]);
        DB::table('categories')->insert([
            'image' => 'data/file/noimage.png',
            'parent_id' => '10',
        ]);
        DB::table('categories')->insert([
            'image' => 'data/file/noimage.png',
            'parent_id' => '10',
        ]);
        DB::table('categories')->insert([
            'image' => 'data/file/noimage.png',
            'parent_id' => '10',
        ]);
    }
}
