<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('category_translations')->truncate();
        DB::table('category_translations')->insert([
            'category_id' => 1,
            'locale' => 'ru',
            'name' => 'Версия процессора',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 1,
            'locale' => 'en',
            'name' => 'Processor version',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 2,
            'locale' => 'ru',
            'name' => 'Intel',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 2,
            'locale' => 'en',
            'name' => 'Intel',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 3,
            'locale' => 'ru',
            'name' => 'Qualcomm',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 3,
            'locale' => 'en',
            'name' => 'Qualcomm',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 4,
            'locale' => 'ru',
            'name' => 'ARM',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 4,
            'locale' => 'en',
            'name' => 'ARM',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 5,
            'locale' => 'ru',
            'name' => 'AMD',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 5,
            'locale' => 'en',
            'name' => 'AMD',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 6,
            'locale' => 'ru',
            'name' => 'Версия ОС',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 6,
            'locale' => 'en',
            'name' => 'OS version',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 7,
            'locale' => 'ru',
            'name' => 'Windows 7',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 7,
            'locale' => 'en',
            'name' => 'Windows 7',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 8,
            'locale' => 'ru',
            'name' => 'Windows 8',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 8,
            'locale' => 'en',
            'name' => 'Windows 8',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 9,
            'locale' => 'ru',
            'name' => 'Windows 10',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 9,
            'locale' => 'en',
            'name' => 'Windows 10',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 10,
            'locale' => 'ru',
            'name' => 'Тип устройства',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 10,
            'locale' => 'en',
            'name' => 'Device type',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 11,
            'locale' => 'ru',
            'name' => 'Одноплатные ПК',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 11,
            'locale' => 'en',
            'name' => 'Single-board PCs',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 12,
            'locale' => 'ru',
            'name' => 'Интернет вещи',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 12,
            'locale' => 'en',
            'name' => 'Internet things',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 13,
            'locale' => 'ru',
            'name' => 'Банкоматы',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 13,
            'locale' => 'en',
            'name' => 'ATMs',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 14,
            'locale' => 'ru',
            'name' => 'Сканеры',
        ]);
        DB::table('category_translations')->insert([
            'category_id' => 14,
            'locale' => 'en',
            'name' => 'Scanners',
        ]);
    }
}
