<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('clients')->truncate();
        DB::table('clients')->insert([
            'name' => 'Atlassian',
            'image' => 'data/clients/atlassian.png',
        ]);
        DB::table('clients')->insert([
            'name' => 'Cisco',
            'image' => 'data/clients/cisco.png',
        ]);
        DB::table('clients')->insert([
            'name' => 'HP',
            'image' => 'data/clients/hp.png',
        ]);
        DB::table('clients')->insert([
            'name' => 'Kaspersky',
            'image' => 'data/clients/kaspersky.png',
        ]);
        DB::table('clients')->insert([
            'name' => 'Microsoft',
            'image' => 'data/clients/microsoft.png',
        ]);
        DB::table('clients')->insert([
            'name' => 'Quest',
            'image' => 'data/clients/quest.png',
        ]);
    }
}
