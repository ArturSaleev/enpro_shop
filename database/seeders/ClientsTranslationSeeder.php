<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientsTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('client_translations')->insert([
            'client_id' => 1,
            'locale' => 'ru',
            'content' => 'Atlassian',
        ]);
        DB::table('client_translations')->insert([
            'client_id' => 1,
            'locale' => 'en',
            'content' => 'Atlassian',
        ]);
        DB::table('client_translations')->insert([
            'client_id' => 2,
            'locale' => 'ru',
            'content' => 'Cisco',
        ]);
        DB::table('client_translations')->insert([
            'client_id' => 2,
            'locale' => 'en',
            'content' => 'Cisco',
        ]);
        DB::table('client_translations')->insert([
            'client_id' => 3,
            'locale' => 'ru',
            'content' => 'HP',
        ]);
        DB::table('client_translations')->insert([
            'client_id' => 3,
            'locale' => 'en',
            'content' => 'HP',
        ]);
        DB::table('client_translations')->insert([
            'client_id' => 4,
            'locale' => 'ru',
            'content' => 'Kaspersky',
        ]);
        DB::table('client_translations')->insert([
            'client_id' => 4,
            'locale' => 'en',
            'content' => 'Kaspersky',
        ]);
        DB::table('client_translations')->insert([
            'client_id' => 5,
            'locale' => 'ru',
            'content' => 'Microsoft',
        ]);
        DB::table('client_translations')->insert([
            'client_id' => 5,
            'locale' => 'en',
            'content' => 'Microsoft',
        ]);
        DB::table('client_translations')->insert([
            'client_id' => 6,
            'locale' => 'ru',
            'content' => 'Quest',
        ]);
        DB::table('client_translations')->insert([
            'client_id' => 6,
            'locale' => 'en',
            'content' => 'Quest',
        ]);
    }
}
