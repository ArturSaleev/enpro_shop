<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('config')->insert([
            'logo' => 'images/logo.png',
            'icon' => 'images/logo_icon.png',
            'email' => 'sales@psolution.ru',
            'phone' => '+7 (343) 226-46-20',
        ]);
    }
}
