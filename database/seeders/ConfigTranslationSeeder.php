<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('config_translations')->insert([
            'config_id' => '1',
            'locale' => 'ru',
            'address' => 'Екатеринбург, ул. Хохрякова 74, оф 1402, +7 (343) 272-78-47'
        ]);
        DB::table('config_translations')->insert([
            'config_id' => '1',
            'locale' => 'ru',
            'address' => 'Москва, Береговой проезд, 5A корпус 1, этаж 11, +7 (495) 127-75-77'
        ]);
        DB::table('config_translations')->insert([
            'config_id' => '1',
            'locale' => 'ru',
            'address' => 'Санкт-Петербург, пр. Непокорённых 49, офис 415, +7 (812) 339-74-56'
        ]);
        DB::table('config_translations')->insert([
            'config_id' => '1',
            'locale' => 'en',
            'address' => 'Ekaterinburg, 74 Khokhryakova St., office 1402, +7 (343) 272-78-47'
        ]);
        DB::table('config_translations')->insert([
            'config_id' => '1',
            'locale' => 'en',
            'address' => 'Moscow, Beregovoy proezd, 5A building 1, floor 11, +7 (495) 127-75-77'
        ]);
        DB::table('config_translations')->insert([
            'config_id' => '1',
            'locale' => 'en',
            'address' => 'St. Petersburg, Nepokornykh pr. 49, office 415, +7 (812) 339-74-56'
        ]);
    }
}
