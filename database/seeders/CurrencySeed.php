<?php

namespace Database\Seeders;

use App\Models\Currency;
use Illuminate\Database\Seeder;

class CurrencySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $c = new Currency();
        $c->code = 'RUB';
        $c->name = 'рубль';
        $c->symbol = 'руб.';
        $c->rate = 1;
        $c->on_default = true;
        $c->position_left = false;
        $c->save();
    }
}
