<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            UserSeed::class,
            CurrencySeed::class,
            LocaleSeed::class,
            ReferencySeed::class,
            ProductSeeder::class,
            ProductImagesSeeder::class,
            ProductTranslationsSeeder::class,
            CategorySeeder::class,
            CategoryTranslationSeeder::class,
            ProductCategorySeeder::class,
            PromotionSeeder::class,
            PromotionTranslationSeeder::class,
            NewsSeeder::class,
            NewsTranslationSeeder::class,
            ClientsSeeder::class,
            ClientsTranslationSeeder::class,
            BannersSeeder::class,
            BannersTranslationSeeder::class,
            AboutSeeder::class,
            AboutTranslationSeeder::class,
            ConfigSeeder::class,
            ConfigTranslationSeeder::class,
            BrandSeeder::class,
            BrandTranslationSeeder::class,
        ]);
    }
}
