<?php

namespace Database\Seeders;

use App\Models\Locale;
use Illuminate\Database\Seeder;

class LocaleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                "code" => "ru",
                "lang_code" => "ru",
                "name" => "Русский",
                "display_name" => "Русский",
                "image" => "",
                "on_default" => 1
            ],
            [
                "code" => "en",
                "lang_code" => "en",
                "name" => "English",
                "display_name" => "English",
                "image" => "",
                "on_default" => 0
            ],
        ];

        foreach($datas as $data){
            Locale::create($data);
        }
    }
}
