<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('news')->truncate();
        DB::table('news')->insert([
            'image' => 'data/news/1.png',
        ]);
        DB::table('news')->insert([
            'image' => 'data/news/2.png',
        ]);
        DB::table('news')->insert([
            'image' => 'data/news/3.png',
        ]);
        DB::table('news')->insert([
            'image' => 'data/news/1.png',
        ]);
        DB::table('news')->insert([
            'image' => 'data/news/2.png',
        ]);
        DB::table('news')->insert([
            'image' => 'data/news/3.png',
        ]);
        DB::table('news')->insert([
            'image' => 'data/news/1.png',
        ]);
    }
}
