<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NewsTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('news_translations')->truncate();
        DB::table('news_translations')->insert([
            'news_id' => 1,
            'locale' => 'ru',
            'title' => 'Microsoft позволит создавать приложения без знания кода',
            'content' => "<p>Задача организации, в особенности же консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Товарищи! реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании позиций.</p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p>"
        ]);
        DB::table('news_translations')->insert([
            'news_id' => 1,
            'locale' => 'en',
            'title' => 'Microsoft will allow you to create applications without knowing the code',
            'content' => "<p>The task of the organization, especially consultation with a wide range of the (specialists) to participate in the formation of the forms of development. Comrades! The realization of the planned of the planned tasks ensures the participation of a wide range of (specialists) in the formation of positions.</p> <p>Day-to-day practice shows that the implementation of the planned targets allows The importance of the system of personnel training, corresponds to the urgent needs. On the other hand on the other hand, the new model of organizational activity represents an interesting experiment testing of new proposals. The diverse and rich experience of the framework and place of personnel training in is largely responsible for the creation of mass participation systems. </p> <p>Day-to-day practice shows that the implementation of the planned targets allows The importance of the system of personnel training, corresponds to the urgent needs. On the other hand on the other hand, the new model of organizational activity represents an interesting experiment testing of new proposals. The diverse and rich experience of the framework and place of personnel training in is largely responsible for the creation of mass participation systems. </p>"
        ]);
        DB::table('news_translations')->insert([
            'news_id' => 2,
            'locale' => 'ru',
            'title' => 'Анонсы конференции Microsoft Build 2021',
            'content' => "<p>Задача организации, в особенности же консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Товарищи! реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании позиций.</p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p>"
        ]);
        DB::table('news_translations')->insert([
            'news_id' => 2,
            'locale' => 'en',
            'title' => 'Microsoft Build 2021 conference announcements',
            'content' => "<p>The task of the organization, especially consultation with a wide range of the (specialists) to participate in the formation of the forms of development. Comrades! The realization of the planned of the planned tasks ensures the participation of a wide range of (specialists) in the formation of positions.</p> <p>Day-to-day practice shows that the implementation of the planned targets allows The importance of the system of personnel training, corresponds to the urgent needs. On the other hand on the other hand, the new model of organizational activity represents an interesting experiment testing of new proposals. The diverse and rich experience of the framework and place of personnel training in is largely responsible for the creation of mass participation systems. </p> <p>Day-to-day practice shows that the implementation of the planned targets allows The importance of the system of personnel training, corresponds to the urgent needs. On the other hand on the other hand, the new model of organizational activity represents an interesting experiment testing of new proposals. The diverse and rich experience of the framework and place of personnel training in is largely responsible for the creation of mass participation systems. </p>"
        ]);
        DB::table('news_translations')->insert([
            'news_id' => 3,
            'locale' => 'ru',
            'title' => 'Gartner назвала Microsoft лидером',
            'content' => "<p>Задача организации, в особенности же консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Товарищи! реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании позиций.</p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p>"
        ]);
        DB::table('news_translations')->insert([
            'news_id' => 3,
            'locale' => 'en',
            'title' => 'Gartner named Microsoft a leader',
            'content' => "<p>The task of the organization, especially consultation with a wide range of the (specialists) to participate in the formation of the forms of development. Comrades! The realization of the planned of the planned tasks ensures the participation of a wide range of (specialists) in the formation of positions.</p> <p>Day-to-day practice shows that the implementation of the planned targets allows The importance of the system of personnel training, corresponds to the urgent needs. On the other hand on the other hand, the new model of organizational activity represents an interesting experiment testing of new proposals. The diverse and rich experience of the framework and place of personnel training in is largely responsible for the creation of mass participation systems. </p> <p>Day-to-day practice shows that the implementation of the planned targets allows The importance of the system of personnel training, corresponds to the urgent needs. On the other hand on the other hand, the new model of organizational activity represents an interesting experiment testing of new proposals. The diverse and rich experience of the framework and place of personnel training in is largely responsible for the creation of mass participation systems. </p>"
        ]);
        DB::table('news_translations')->insert([
            'news_id' => 4,
            'locale' => 'ru',
            'title' => 'Microsoft позволит создавать приложения без знания кода',
            'content' => "<p>Задача организации, в особенности же консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Товарищи! реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании позиций.</p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p>"
        ]);
        DB::table('news_translations')->insert([
            'news_id' => 4,
            'locale' => 'en',
            'title' => 'Microsoft will allow you to create applications without knowing the code',
            'content' => "<p>The task of the organization, especially consultation with a wide range of the (specialists) to participate in the formation of the forms of development. Comrades! The realization of the planned of the planned tasks ensures the participation of a wide range of (specialists) in the formation of positions.</p> <p>Day-to-day practice shows that the implementation of the planned targets allows The importance of the system of personnel training, corresponds to the urgent needs. On the other hand on the other hand, the new model of organizational activity represents an interesting experiment testing of new proposals. The diverse and rich experience of the framework and place of personnel training in is largely responsible for the creation of mass participation systems. </p> <p>Day-to-day practice shows that the implementation of the planned targets allows The importance of the system of personnel training, corresponds to the urgent needs. On the other hand on the other hand, the new model of organizational activity represents an interesting experiment testing of new proposals. The diverse and rich experience of the framework and place of personnel training in is largely responsible for the creation of mass participation systems. </p>"
        ]);
        DB::table('news_translations')->insert([
            'news_id' => 5,
            'locale' => 'ru',
            'title' => 'Анонсы конференции Microsoft Build 2021',
            'content' => "<p>Задача организации, в особенности же консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Товарищи! реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании позиций.</p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p>"
        ]);
        DB::table('news_translations')->insert([
            'news_id' => 5,
            'locale' => 'en',
            'title' => 'Microsoft Build 2021 conference announcements',
            'content' => "<p>The task of the organization, especially consultation with a wide range of the (specialists) to participate in the formation of the forms of development. Comrades! The realization of the planned of the planned tasks ensures the participation of a wide range of (specialists) in the formation of positions.</p> <p>Day-to-day practice shows that the implementation of the planned targets allows The importance of the system of personnel training, corresponds to the urgent needs. On the other hand on the other hand, the new model of organizational activity represents an interesting experiment testing of new proposals. The diverse and rich experience of the framework and place of personnel training in is largely responsible for the creation of mass participation systems. </p> <p>Day-to-day practice shows that the implementation of the planned targets allows The importance of the system of personnel training, corresponds to the urgent needs. On the other hand on the other hand, the new model of organizational activity represents an interesting experiment testing of new proposals. The diverse and rich experience of the framework and place of personnel training in is largely responsible for the creation of mass participation systems. </p>"
        ]);
        DB::table('news_translations')->insert([
            'news_id' => 6,
            'locale' => 'ru',
            'title' => 'Gartner назвала Microsoft лидером',
            'content' => "<p>Задача организации, в особенности же консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Товарищи! реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании позиций.</p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p>"
        ]);
        DB::table('news_translations')->insert([
            'news_id' => 6,
            'locale' => 'en',
            'title' => 'Gartner named Microsoft a leader',
            'content' => "<p>The task of the organization, especially consultation with a wide range of the (specialists) to participate in the formation of the forms of development. Comrades! The realization of the planned of the planned tasks ensures the participation of a wide range of (specialists) in the formation of positions.</p> <p>Day-to-day practice shows that the implementation of the planned targets allows The importance of the system of personnel training, corresponds to the urgent needs. On the other hand on the other hand, the new model of organizational activity represents an interesting experiment testing of new proposals. The diverse and rich experience of the framework and place of personnel training in is largely responsible for the creation of mass participation systems. </p> <p>Day-to-day practice shows that the implementation of the planned targets allows The importance of the system of personnel training, corresponds to the urgent needs. On the other hand on the other hand, the new model of organizational activity represents an interesting experiment testing of new proposals. The diverse and rich experience of the framework and place of personnel training in is largely responsible for the creation of mass participation systems. </p>"
        ]);
        DB::table('news_translations')->insert([
            'news_id' => 7,
            'locale' => 'ru',
            'title' => 'Gartner назвала Microsoft лидером',
            'content' => "<p>Задача организации, в особенности же консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Товарищи! реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании позиций.</p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p>"
        ]);
        DB::table('news_translations')->insert([
            'news_id' => 7,
            'locale' => 'en',
            'title' => 'Gartner named Microsoft a leader',
            'content' => "<p>The task of the organization, especially consultation with a wide range of the (specialists) to participate in the formation of the forms of development. Comrades! The realization of the planned of the planned tasks ensures the participation of a wide range of (specialists) in the formation of positions.</p> <p>Day-to-day practice shows that the implementation of the planned targets allows The importance of the system of personnel training, corresponds to the urgent needs. On the other hand on the other hand, the new model of organizational activity represents an interesting experiment testing of new proposals. The diverse and rich experience of the framework and place of personnel training in is largely responsible for the creation of mass participation systems. </p> <p>Day-to-day practice shows that the implementation of the planned targets allows The importance of the system of personnel training, corresponds to the urgent needs. On the other hand on the other hand, the new model of organizational activity represents an interesting experiment testing of new proposals. The diverse and rich experience of the framework and place of personnel training in is largely responsible for the creation of mass participation systems. </p>"
        ]);
    }
}
