<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('product_images')->truncate();
        DB::table('product_images')->insert([
            'product_id' => 1,
            'image' => 'data/products/1.png',
            'on_main' => 1,
            'on_description' => 1,
            'on_requirement' => 1,
            'on_version' => 1,
            'on_buy_description' => 1,
        ]);
        DB::table('product_images')->insert([
            'product_id' => 2,
            'image' => 'data/products/2.png',
            'on_main' => 1,
            'on_description' => 1,
            'on_requirement' => 1,
            'on_version' => 1,
            'on_buy_description' => 1,
        ]);
        DB::table('product_images')->insert([
            'product_id' => 3,
            'image' => 'data/products/3.png',
            'on_main' => 1,
            'on_description' => 1,
            'on_requirement' => 1,
            'on_version' => 1,
            'on_buy_description' => 1,
        ]);
        DB::table('product_images')->insert([
            'product_id' => 4,
            'image' => 'data/products/4.png',
            'on_main' => 1,
            'on_description' => 1,
            'on_requirement' => 1,
            'on_version' => 1,
            'on_buy_description' => 1,
        ]);
        DB::table('product_images')->insert([
            'product_id' => 5,
            'image' => 'data/products/5.png',
            'on_main' => 1,
            'on_description' => 1,
            'on_requirement' => 1,
            'on_version' => 1,
            'on_buy_description' => 1,
        ]);
        DB::table('product_images')->insert([
            'product_id' => 6,
            'image' => 'data/products/6.png',
            'on_main' => 1,
            'on_description' => 1,
            'on_requirement' => 1,
            'on_version' => 1,
            'on_buy_description' => 1,
        ]);
    }
}
