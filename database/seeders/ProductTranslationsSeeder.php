<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductTranslationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('product_translations')->truncate();
        DB::table('product_translations')->insert([
            'product_id' => 1,
            'locale' => 'ru',
            'name' => 'Windows 10 IoT Базовая',
            'meta' => 'Создана для небольших, безопасных и интеллектуальных устройств.',
            'description' => "<div class=\"container-fluid\" style=\" background-image: url('@productimage@'); background-repeat: no-repeat; background-size: 48%; background-color: #EBF7FF; background-position: right; min-height: 20vw; \"> <div class=\"container py-6\" style=\"height: 546px;\"> <div class=\"row\"> <div class=\"col-lg-6\"> <p><h4>@productname@</h4></p> <p> Создана для небольших, безопасных и интеллектуальных устройств. Самая маленькая версия операционной системы Windows 10 обладает всеми возможностями управления и безопасности, которых вы можете ожидать от Windows 10. </p> </div> </div> </div> </div> <div class=\"container\"> <p><h4>Ключевые преимущества перед \"настольными\" версиями</h4></p> <p> <ul> <li>В 2–3 раза дешевле настольной (обычной) Windows 10 Pro (в зависимости от используемого процессора);</li> <li>Полная совместимость с приложениями и устройствами для Windows 10;</li> <li>Не получает автоматических обновлений версий;</li> <li>Защищена от нежелательных действий персонала или злоумышленников;</li> <li>Цена зависит от модели процессора в устройстве (мощнее - дороже, слабее - дешевле);</li> <li>Длительный жизненный цикл (поддержка и доступность каждой сборки LTSC - 10 лет);</li> <li>Готовый дистрибутив — экономите на обучении сотрудников и сборке собственного образа;</li> <li>Можно включить нулевой уровень телеметрии, на котором ОС отправляет минимум данных.</li> </ul> </p> </div>",
            'requirement' => "<div class=\"container-fluid\" style=\" background-image: url('@productimage@'); background-repeat: no-repeat; background-size: 48%; background-color: #EBF7FF; background-position: right; min-height: 20vw; \"> <div class=\"container py-6\" style=\"height: 546px;\"> <div class=\"row\"> <div class=\"col-lg-6\"> <p><h4>@productname@</h4></p> <p> Создана для небольших, безопасных и интеллектуальных устройств. Самая маленькая версия операционной системы Windows 10 обладает всеми возможностями управления и безопасности, которых вы можете ожидать от Windows 10. </p> </div> </div> </div> </div> <div class=\"container\"> <p><h4>Ключевые преимущества перед \"настольными\" версиями</h4></p> <p> <ul> <li>В 2–3 раза дешевле настольной (обычной) Windows 10 Pro (в зависимости от используемого процессора);</li> <li>Полная совместимость с приложениями и устройствами для Windows 10;</li> <li>Не получает автоматических обновлений версий;</li> <li>Защищена от нежелательных действий персонала или злоумышленников;</li> <li>Цена зависит от модели процессора в устройстве (мощнее - дороже, слабее - дешевле);</li> <li>Длительный жизненный цикл (поддержка и доступность каждой сборки LTSC - 10 лет);</li> <li>Готовый дистрибутив — экономите на обучении сотрудников и сборке собственного образа;</li> <li>Можно включить нулевой уровень телеметрии, на котором ОС отправляет минимум данных.</li> </ul> </p> </div>",
            'version' => "<div class=\"container-fluid\" style=\" background-image: url('@productimage@'); background-repeat: no-repeat; background-size: 48%; background-color: #EBF7FF; background-position: right; min-height: 20vw; \"> <div class=\"container py-6\" style=\"height: 546px;\"> <div class=\"row\"> <div class=\"col-lg-6\"> <p><h4>@productname@</h4></p> <p> Создана для небольших, безопасных и интеллектуальных устройств. Самая маленькая версия операционной системы Windows 10 обладает всеми возможностями управления и безопасности, которых вы можете ожидать от Windows 10. </p> </div> </div> </div> </div> <div class=\"container\"> <p><h4>Ключевые преимущества перед \"настольными\" версиями</h4></p> <p> <ul> <li>В 2–3 раза дешевле настольной (обычной) Windows 10 Pro (в зависимости от используемого процессора);</li> <li>Полная совместимость с приложениями и устройствами для Windows 10;</li> <li>Не получает автоматических обновлений версий;</li> <li>Защищена от нежелательных действий персонала или злоумышленников;</li> <li>Цена зависит от модели процессора в устройстве (мощнее - дороже, слабее - дешевле);</li> <li>Длительный жизненный цикл (поддержка и доступность каждой сборки LTSC - 10 лет);</li> <li>Готовый дистрибутив — экономите на обучении сотрудников и сборке собственного образа;</li> <li>Можно включить нулевой уровень телеметрии, на котором ОС отправляет минимум данных.</li> </ul> </p> </div>",
            'buy_description' => "<div class=\"container-fluid\" style=\" background-image: url('@productimage@'); background-repeat: no-repeat; background-size: 48%; background-color: #EBF7FF; background-position: right; min-height: 20vw; \"> <div class=\"container py-6\" style=\"height: 546px;\"> <div class=\"row\"> <div class=\"col-lg-6\"> <p><h4>@productname@</h4></p> <p> Создана для небольших, безопасных и интеллектуальных устройств. Самая маленькая версия операционной системы Windows 10 обладает всеми возможностями управления и безопасности, которых вы можете ожидать от Windows 10. </p> </div> </div> </div> </div> <div class=\"container\"> <p><h4>Ключевые преимущества перед \"настольными\" версиями</h4></p> <p> <ul> <li>В 2–3 раза дешевле настольной (обычной) Windows 10 Pro (в зависимости от используемого процессора);</li> <li>Полная совместимость с приложениями и устройствами для Windows 10;</li> <li>Не получает автоматических обновлений версий;</li> <li>Защищена от нежелательных действий персонала или злоумышленников;</li> <li>Цена зависит от модели процессора в устройстве (мощнее - дороже, слабее - дешевле);</li> <li>Длительный жизненный цикл (поддержка и доступность каждой сборки LTSC - 10 лет);</li> <li>Готовый дистрибутив — экономите на обучении сотрудников и сборке собственного образа;</li> <li>Можно включить нулевой уровень телеметрии, на котором ОС отправляет минимум данных.</li> </ul> </p> </div>",
        ]);
        DB::table('product_translations')->insert([
            'product_id' => 1,
            'locale' => 'en',
            'name' => 'Windows 10 IoT Basic',
            'meta' => 'Designed for small, safe and smart devices.',
            'description' => "<div class=\"container-fluid\" style=\" background-image: url('@productimage@'); background-repeat: no-repeat; background-size: 48%; background-color: #EBF7FF; background-position: right; min-height: 20vw; \"> <div class=\"container py-6\"> <div class=\"row\"> <div class=\"col-lg-6\"> <p><h4>@productname@</h4></p> <p> Designed for small, secure and intelligent devices. The smallest version the Windows 10 operating system has all the management and security features, which you can expect from Windows 10. </p> </div> </div> </div> </div> <div class=\"container\"> <p><h4>Key advantages over the \"desktop\" versions</h4></p> <p> <ul> <li>2-3 times cheaper than desktop (regular) Windows 10 Pro (depending on the processor used);</li> <li>Full compatibility with applications and devices for Windows 10;</li> <li>Does not receive automatic version updates;</li> <li>Is protected from unwanted actions of personnel or intruders;</li> <li>The price depends on the processor model in the device (more powerful - more expensive, weaker-cheaper);</li> <li>Long life cycle (support and availability of each LTSC assembly - 10 years);</li> <li>Ready-made distribution-save on training employees and building your own image;</li> <li>You can enable the zero level of telemetry, at which the OS sends a minimum of data.</li> </ul> </p> </div>",
            'requirement' => "<div class=\"container-fluid\" style=\" background-image: url('@productimage@'); background-repeat: no-repeat; background-size: 48%; background-color: #EBF7FF; background-position: right; min-height: 20vw; \"> <div class=\"container py-6\"> <div class=\"row\"> <div class=\"col-lg-6\"> <p><h4>@productname@</h4></p> <p> Designed for small, secure and intelligent devices. The smallest version the Windows 10 operating system has all the management and security features, which you can expect from Windows 10. </p> </div> </div> </div> </div> <div class=\"container\"> <p><h4>Key advantages over the \"desktop\" versions</h4></p> <p> <ul> <li>2-3 times cheaper than desktop (regular) Windows 10 Pro (depending on the processor used);</li> <li>Full compatibility with applications and devices for Windows 10;</li> <li>Does not receive automatic version updates;</li> <li>Is protected from unwanted actions of personnel or intruders;</li> <li>The price depends on the processor model in the device (more powerful - more expensive, weaker-cheaper);</li> <li>Long life cycle (support and availability of each LTSC assembly - 10 years);</li> <li>Ready-made distribution-save on training employees and building your own image;</li> <li>You can enable the zero level of telemetry, at which the OS sends a minimum of data.</li> </ul> </p> </div>",
            'version' => "<div class=\"container-fluid\" style=\" background-image: url('@productimage@'); background-repeat: no-repeat; background-size: 48%; background-color: #EBF7FF; background-position: right; min-height: 20vw; \"> <div class=\"container py-6\"> <div class=\"row\"> <div class=\"col-lg-6\"> <p><h4>@productname@</h4></p> <p> Designed for small, secure and intelligent devices. The smallest version the Windows 10 operating system has all the management and security features, which you can expect from Windows 10. </p> </div> </div> </div> </div> <div class=\"container\"> <p><h4>Key advantages over the \"desktop\" versions</h4></p> <p> <ul> <li>2-3 times cheaper than desktop (regular) Windows 10 Pro (depending on the processor used);</li> <li>Full compatibility with applications and devices for Windows 10;</li> <li>Does not receive automatic version updates;</li> <li>Is protected from unwanted actions of personnel or intruders;</li> <li>The price depends on the processor model in the device (more powerful - more expensive, weaker-cheaper);</li> <li>Long life cycle (support and availability of each LTSC assembly - 10 years);</li> <li>Ready-made distribution-save on training employees and building your own image;</li> <li>You can enable the zero level of telemetry, at which the OS sends a minimum of data.</li> </ul> </p> </div>",
            'buy_description' => "<div class=\"container-fluid\" style=\" background-image: url('@productimage@'); background-repeat: no-repeat; background-size: 48%; background-color: #EBF7FF; background-position: right; min-height: 20vw; \"> <div class=\"container py-6\"> <div class=\"row\"> <div class=\"col-lg-6\"> <p><h4>@productname@</h4></p> <p> Designed for small, secure and intelligent devices. The smallest version the Windows 10 operating system has all the management and security features, which you can expect from Windows 10. </p> </div> </div> </div> </div> <div class=\"container\"> <p><h4>Key advantages over the \"desktop\" versions</h4></p> <p> <ul> <li>2-3 times cheaper than desktop (regular) Windows 10 Pro (depending on the processor used);</li> <li>Full compatibility with applications and devices for Windows 10;</li> <li>Does not receive automatic version updates;</li> <li>Is protected from unwanted actions of personnel or intruders;</li> <li>The price depends on the processor model in the device (more powerful - more expensive, weaker-cheaper);</li> <li>Long life cycle (support and availability of each LTSC assembly - 10 years);</li> <li>Ready-made distribution-save on training employees and building your own image;</li> <li>You can enable the zero level of telemetry, at which the OS sends a minimum of data.</li> </ul> </p> </div>",
        ]);
        DB::table('product_translations')->insert([
            'product_id' => 2,
            'locale' => 'ru',
            'name' => 'Windows 10 IoT Корпоративная',
            'meta' => 'Позволяет перенести всю мощь Windows в самые современные интеллектуальные устройства',
        ]);
        DB::table('product_translations')->insert([
            'product_id' => 2,
            'locale' => 'en',
            'name' => 'Windows 10 IoT Enterprise',
            'meta' => 'Allows you to bring all the power of Windows to the most advanced smart devices',
        ]);
        DB::table('product_translations')->insert([
            'product_id' => 3,
            'locale' => 'ru',
            'name' => 'Windows Server IoT 2019',
            'meta' => 'Устройства, использующие Windows Server IoT 2019, способны справляться с большими рабочими нагрузками',
        ]);
        DB::table('product_translations')->insert([
            'product_id' => 3,
            'locale' => 'en',
            'name' => 'Windows Server IoT 2019',
            'meta' => 'Devices using Windows Server IoT 2019 can handle heavy workloads',
        ]);
        DB::table('product_translations')->insert([
            'product_id' => 4,
            'locale' => 'ru',
            'name' => 'SQL Server IoT 2019',
            'meta' => 'SQL Server IoT 2019 обеспечивает мощную аналитику и масштабные возможности хранения',
        ]);
        DB::table('product_translations')->insert([
            'product_id' => 4,
            'locale' => 'en',
            'name' => 'SQL Server IoT 2019',
            'meta' => 'SQL Server IoT 2019 provides powerful analytics and large-scale storage capabilities',
        ]);
        DB::table('product_translations')->insert([
            'product_id' => 5,
            'locale' => 'ru',
            'name' => 'SQL Server IoT 2019',
            'meta' => 'SQL Server IoT 2019 обеспечивает мощную аналитику и масштабные возможности хранения',
        ]);
        DB::table('product_translations')->insert([
            'product_id' => 5,
            'locale' => 'en',
            'name' => 'SQL Server IoT 2019',
            'meta' => 'SQL Server IoT 2019 provides powerful analytics and large-scale storage capabilities',
        ]);
        DB::table('product_translations')->insert([
            'product_id' => 6,
            'locale' => 'ru',
            'name' => 'SQL Server IoT 2019',
            'meta' => 'SQL Server IoT 2019 обеспечивает мощную аналитику и масштабные возможности хранения',
        ]);
        DB::table('product_translations')->insert([
            'product_id' => 6,
            'locale' => 'en',
            'name' => 'SQL Server IoT 2019',
            'meta' => 'SQL Server IoT 2019 provides powerful analytics and large-scale storage capabilities',
        ]);
    }
}
