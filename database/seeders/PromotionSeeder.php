<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('promotion')->truncate();
        DB::table('promotion')->insert([
            'image' => 'data/stocks/1.png',
        ]);
        DB::table('promotion')->insert([
            'image' => 'data/stocks/2.png',
        ]);
        DB::table('promotion')->insert([
            'image' => 'data/stocks/3.png',
        ]);
        DB::table('promotion')->insert([
            'image' => 'data/stocks/1.png',
        ]);
    }
}
