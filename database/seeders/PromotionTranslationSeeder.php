<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PromotionTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('promotion_translations')->truncate();
        DB::table('promotion_translations')->insert([
            'promotion_id' => 1,
            'locale' => 'ru',
            'title' => 'Скидка 35% при покупке от 5 лицензии',
            'content' => "<p>Задача организации, в особенности же консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Товарищи! реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании позиций.</p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p>",
        ]);
        DB::table('promotion_translations')->insert([
            'promotion_id' => 1,
            'locale' => 'en',
            'title' => '35% discount when buying from 5 licenses',
            'content' => "<p>The task of the organization, in particular, consultation with a wide asset provides a wide range of (specialists) participation in the formation of forms of development. Comrades! the implementation of the planned planned tasks provides a wide range of (specialists) participation in the formation of positions.</p> <p>Everyday practice shows that the implementation of the planned planned tasks allows us to assess the value of the personnel training system, meets the urgent needs. On the other hand, the new model of organizational activity is an interesting experiment in testing new proposals. The diverse and rich experience of the framework and the place of training of personnel largely determines the creation of mass participation systems.</p> <p>Everyday practice shows that the implementation of the planned planned tasks allows us to assess the value of the personnel training system, meets the urgent needs. On the other hand, the new model of organizational activity is an interesting experiment in testing new proposals. The diverse and rich experience of the framework and the place of training of personnel largely determines the creation of mass participation systems.</p>"
        ]);
        DB::table('promotion_translations')->insert([
            'promotion_id' => 2,
            'locale' => 'ru',
            'title' => 'Скидка 35% при покупке от 5 лицензии',
            'content' => "<p>Задача организации, в особенности же консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Товарищи! реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании позиций.</p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p>",
        ]);
        DB::table('promotion_translations')->insert([
            'promotion_id' => 2,
            'locale' => 'en',
            'title' => '35% discount when buying from 5 licenses',
            'content' => "<p>The task of the organization, in particular, consultation with a wide asset provides a wide range of (specialists) participation in the formation of forms of development. Comrades! the implementation of the planned planned tasks provides a wide range of (specialists) participation in the formation of positions.</p> <p>Everyday practice shows that the implementation of the planned planned tasks allows us to assess the value of the personnel training system, meets the urgent needs. On the other hand, the new model of organizational activity is an interesting experiment in testing new proposals. The diverse and rich experience of the framework and the place of training of personnel largely determines the creation of mass participation systems.</p> <p>Everyday practice shows that the implementation of the planned planned tasks allows us to assess the value of the personnel training system, meets the urgent needs. On the other hand, the new model of organizational activity is an interesting experiment in testing new proposals. The diverse and rich experience of the framework and the place of training of personnel largely determines the creation of mass participation systems.</p>"
        ]);
        DB::table('promotion_translations')->insert([
            'promotion_id' => 3,
            'locale' => 'ru',
            'title' => 'Скидка 35% при покупке от 5 лицензии',
            'content' => "<p>Задача организации, в особенности же консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Товарищи! реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании позиций.</p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p>",
        ]);
        DB::table('promotion_translations')->insert([
            'promotion_id' => 3,
            'locale' => 'en',
            'title' => '35% discount when buying from 5 licenses',
            'content' => "<p>The task of the organization, in particular, consultation with a wide asset provides a wide range of (specialists) participation in the formation of forms of development. Comrades! the implementation of the planned planned tasks provides a wide range of (specialists) participation in the formation of positions.</p> <p>Everyday practice shows that the implementation of the planned planned tasks allows us to assess the value of the personnel training system, meets the urgent needs. On the other hand, the new model of organizational activity is an interesting experiment in testing new proposals. The diverse and rich experience of the framework and the place of training of personnel largely determines the creation of mass participation systems.</p> <p>Everyday practice shows that the implementation of the planned planned tasks allows us to assess the value of the personnel training system, meets the urgent needs. On the other hand, the new model of organizational activity is an interesting experiment in testing new proposals. The diverse and rich experience of the framework and the place of training of personnel largely determines the creation of mass participation systems.</p>"
        ]);
        DB::table('promotion_translations')->insert([
            'promotion_id' => 4,
            'locale' => 'ru',
            'title' => 'Скидка 35% при покупке от 5 лицензии',
            'content' => "<p>Задача организации, в особенности же консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Товарищи! реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании позиций.</p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p> <p>Повседневная практика показывает, что реализация намеченных плановых заданий позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. С другой стороны новая модель организационной деятельности представляет собой интересный эксперимент проверки новых предложений. Разнообразный и богатый опыт рамки и место обучения кадров в значительной степени обуславливает создание систем массового участия. </p>",
        ]);
        DB::table('promotion_translations')->insert([
            'promotion_id' => 4,
            'locale' => 'en',
            'title' => '35% discount when buying from 5 licenses',
            'content' => "<p>The task of the organization, in particular, consultation with a wide asset provides a wide range of (specialists) participation in the formation of forms of development. Comrades! the implementation of the planned planned tasks provides a wide range of (specialists) participation in the formation of positions.</p> <p>Everyday practice shows that the implementation of the planned planned tasks allows us to assess the value of the personnel training system, meets the urgent needs. On the other hand, the new model of organizational activity is an interesting experiment in testing new proposals. The diverse and rich experience of the framework and the place of training of personnel largely determines the creation of mass participation systems.</p> <p>Everyday practice shows that the implementation of the planned planned tasks allows us to assess the value of the personnel training system, meets the urgent needs. On the other hand, the new model of organizational activity is an interesting experiment in testing new proposals. The diverse and rich experience of the framework and the place of training of personnel largely determines the creation of mass participation systems.</p>"
        ]);
    }
}
