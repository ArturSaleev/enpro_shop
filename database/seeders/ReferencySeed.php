<?php

namespace Database\Seeders;

use App\Models\RefLength;
use App\Models\RefWeight;
use Illuminate\Database\Seeder;

class ReferencySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data_length = [
            [
                "ru" => [
                    "name" => "Миллиметр",
                    "abbreviate" => "мм"
                ],
                "en" => [
                    "name" => "Millimeter",
                    "abbreviate" => "mm"
                ]
            ],
            [
                "ru" => [
                    "name" => "Сантиметр",
                    "abbreviate" => "см"
                ],
                "en" => [
                    "name" => "Centimeter",
                    "abbreviate" => "cm"
                ]
            ],
            [
                "ru" => [
                    "name" => "Дециметр",
                    "abbreviate" => "дм"
                ],
                "en" => [
                    "name" => "Decimeter",
                    "abbreviate" => "dm"
                ]
            ],
            [
                "ru" => [
                    "name" => "Метр",
                    "abbreviate" => "м"
                ],
                "en" => [
                    "name" => "Meter",
                    "abbreviate" => "m"
                ]
            ],
        ];

        $data_wight = [
            [
                "ru" => [
                    "name" => "Килограмм",
                    "abbreviate" => "кг"
                ],
                "en" => [
                    "name" => "Kilogram",
                    "abbreviate" => "kg"
                ]
            ],
            [
                "ru" => [
                    "name" => "Грамм",
                    "abbreviate" => "г"
                ],
                "en" => [
                    "name" => "Gram",
                    "abbreviate" => "g"
                ]
            ],
            [
                "ru" => [
                    "name" => "Миллиграмм",
                    "abbreviate" => "мг"
                ],
                "en" => [
                    "name" => "Milligram",
                    "abbreviate" => "mg"
                ]
            ],
            [
                "ru" => [
                    "name" => "Центнер",
                    "abbreviate" => "ц"
                ],
                "en" => [
                    "name" => "Hundredweight",
                    "abbreviate" => "q"
                ]
            ],
            [
                "ru" => [
                    "name" => "Тонна",
                    "abbreviate" => "т"
                ],
                "en" => [
                    "name" => "Ton",
                    "abbreviate" => "t"
                ]
            ],

        ];

        foreach($data_length as $data){
            RefLength::create($data);
        }

        foreach($data_wight as $data){
            RefWeight::create($data);
        }
    }
}
