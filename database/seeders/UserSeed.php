<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            "email" => "admin@mail.ru",
            "password" => Hash::make('12345678'),
            "name" => "Admin",
            "is_admin" => true
        ];

        DB::table('users')->insert($data);
    }
}
