// SideNav Initialization
$(".button-collapse").sideNav();

var container = document.querySelector('.custom-scrollbar');
var ps = new PerfectScrollbar(container, {
    wheelSpeed: 2,
    wheelPropagation: true,
    minScrollbarLength: 20
});

// Data Picker Initialization
$('.datepicker').pickadate();

// Material Select Initialization
$(document).ready(function () {
    $('.mdb-select').material_select();
});

// Tooltips Initialization
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})


// Small chart
$(function () {
    $('.min-chart#chart-sales').easyPieChart({
        barColor: "#FF5252",
        onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });
});

// Main chart
/*
var ctxL = document.getElementById("lineChart").getContext('2d');
var myLineChart = new Chart(ctxL, {
    type: 'line',
    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
            label: "My First dataset",
            fillColor: "#fff",
            backgroundColor: 'rgba(255, 255, 255, .3)',
            borderColor: 'rgba(255, 255, 255)',
            data: [0, 10, 5, 2, 20, 30, 45],
        }]
    },
    options: {
        legend: {
            labels: {
                fontColor: "#fff",
            }
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: true,
                    color: "rgba(255,255,255,.25)"
                },
                ticks: {
                    fontColor: "#fff",
                },
            }],
            yAxes: [{
                display: true,
                gridLines: {
                    display: true,
                    color: "rgba(255,255,255,.25)"
                },
                ticks: {
                    fontColor: "#fff",
                },
            }],
        }
    }
});
*/
$(function () {
    $('#dark-mode').on('click', function (e) {

        e.preventDefault();
        $('h4, button').not('.check').toggleClass('dark-grey-text text-white');
        $('.list-panel a').toggleClass('dark-grey-text');

        $('footer, .card').toggleClass('dark-card-admin');
        $('body, .navbar').toggleClass('white-skin navy-blue-skin');
        $(this).toggleClass('white text-dark btn-outline-black');
        $('body').toggleClass('dark-bg-admin');
        $('h6, .card, p, td, th, i, li a, h4, input, label').not(
            '#slide-out i, ' +
            '#slide-out a, ' +
            '.dropdown-item i, ' +
            '.dropdown-item, '+
            'input[class=form-control]'
        ).toggleClass('text-white');
        $('.btn-dash').toggleClass('grey blue').toggleClass('lighten-3 darken-3');
        $('.gradient-card-header').toggleClass('white black lighten-4');
        $('.list-panel a').toggleClass('navy-blue-bg-a text-white').toggleClass('list-group-border');

    });
});

function formValidate() {
    let inputs = document.querySelectorAll('.needs-validation');
    let alert = document.querySelector('.error_required_alert');
    let result = false;
        inputs.forEach(input => {
            let value = input.value
            if (input.classList.contains('editor') && input.style.display === 'none') {
                value = input.nextSibling.childNodes[1].childNodes[1].childNodes[1].contentDocument.querySelector('body').innerText
            }
            if (value === null || value.trim() === '') {
                if (alert) {
                    alert.classList.add('show')
                    setTimeout(() => {
                        alert.classList.remove('show')
                    }, 2000)
                }
                if (input.classList.contains('editor') && input.style.display === 'none') {
                    input = input.nextSibling
                }
                !input.classList.contains('border-danger') && input.classList.add('border-danger')
                if (!input.nextElementSibling || !input.nextElementSibling.classList.contains('error_required')) {
                    let requiredMessage = document.createElement('small')
                    requiredMessage.innerText = 'Не заполнено!'
                    requiredMessage.classList.add('text-danger')
                    requiredMessage.classList.add('error_required')
                    input.after(requiredMessage)
                }
                result = false
            } else {
                result = true
            }
        })
        return result
}

let showLimitedContainer = document.querySelector('.show_limited')
if (showLimitedContainer) {
    HTMLCollection.prototype.map = Array.prototype.map
    showLimitedContainer.children.map((el, key) => {
        if (el.tagName !== 'BUTTON') {
            if (key > 2) {
                el.classList.add('collapse')
                el.setAttribute('id', 'collapseCard')
            }
        }
    })
}

$('.address').click(function(){
    let l = $(this).attr('data');
    let id = Math.round(Math.random() * 100000);
    let html = '<div class="input-group mb-3" id="'+id+'">\n' +
        '<input type="text" class="form-control" name="address['+l+'][]">\n' +
        '<span class="input-group-text btn-danger del_address" data="'+id+'"><i class="fas fa-trash"></i></span>\n' +
        '</div>';
    $('#address_'+l).append(html);
})

function deleteAdress($id){
    $('#'+$id).remove();
}
