<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'mainpage' => 'Home page',
    'searchbar' => 'Search by SKU...',
    'filterheader' => 'Filters',
    'resethardfilter' => 'Reset all filters',
    'resetfilter' => 'Reset filters',
    'details' => 'Read more',
    'productnav' => 'Products',
    'aboutusnav' => 'About us',
    'newsnav' => 'News',
    'stocknav' => 'Promotions',
    'tabsproduct1' => 'Description',
    'tabsproduct2' => 'Min. requirements.',
    'tabsproduct3' => 'Versions',
    'tabsproduct4' => 'Buy',
    'buttonbuy' => 'Buy',
    'orderversion' => 'Version',
    'quantityversion' => 'Quantity',
    'detailsversion' => 'Way to contact with you',
    'contactversion' => 'Phone number or email',
    'homefeaturetitle1' => 'Guarantee',
    'homefeaturecontent1' => 'Certified by Microsoft Corporation',
    'homefeaturetitle2' => 'Fast shipping',
    'homefeaturecontent2' => 'Delivery takes from 2 days to one week',
    'homefeaturetitle3' => 'Individual discounts',
    'homefeaturecontent3' => 'By agreement we can provide discounts when ordering from 50 items',
    'ourclients' => 'Our Clients',
    'adminpanel' => 'Admin panel',
    'promotion' => 'Promotion',

];
