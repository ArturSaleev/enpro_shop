<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'Login' => "Login",
    'login' => "login",
    'logout' => "Logout",
    'label_email' => "E-Mail Address",
    'label_password' => 'Password',
    'label_confirm_password' => 'Confirm Password',
    'label_remember' => 'Remember Me',
    'label_forgot' => 'Forgot Your Password?',
    'label_register' => 'Register',
    'label_name' => 'Name',
    'register' => 'Register',
    'label_reset_password' => 'Reset Password',
    'send_link_password' => 'Send Password Reset Link',
];
