<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'mainpage' => 'Главная страница',
    'searchbar' => 'Поиск по SKU...',
    'filterheader' => 'Фильтры',
    'resethardfilter' => 'Сбросить все фильтры',
    'resetfilter' => 'Сбросить фильтры',
    'details' => 'Подробнее',
    'productnav' => 'Продукты',
    'aboutusnav' => 'О нас',
    'newsnav' => 'Новости',
    'stocknav' => 'Акции',
    'tabsproduct1' => 'Описание',
    'tabsproduct2' => 'Мин. требования',
    'tabsproduct3' => 'Версии',
    'tabsproduct4' => 'Купить',
    'buttonbuy' => 'Заказать',
    'orderversion' => 'Версия',
    'quantityversion' => 'Кол-во',
    'detailsversion' => 'Как к вам обращаться?',
    'contactversion' => 'Номер телефона или почта',
    'homefeaturetitle1' => 'Гарантия',
    'homefeaturecontent1' => 'Товар сертифицирован корпорацией Microsoft',
    'homefeaturetitle2' => 'Быстрая доставка',
    'homefeaturecontent2' => 'Доставка занимает от 2 дней до одной недели',
    'homefeaturetitle3' => 'Индивидуальные скидки',
    'homefeaturecontent3' => 'По согласованию можем предоставить скидки при заказе от 50 позиций',
    'ourclients' => 'Наши Клиенты',
    'adminpanel' => 'Админ панель',
    'promotion' => 'Акция',
];
