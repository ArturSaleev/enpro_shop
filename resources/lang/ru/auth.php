<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Эти учетные данные не соответствуют нашим записям.',
    'password' => 'Указанный пароль неверен.',
    'throttle' => 'Слишком много попыток входа в систему. Пожалуйста, повторите попытку через :seconds секунд.',
    'Login' => "Авторизация",
    'login' => "Войти",
    'logout' => "Выход",
    'label_email' => "E-Mail адрес",
    'label_password' => 'Пароль',
    'label_confirm_password' => 'Еще раз пароль',
    'label_remember' => 'Запомнить',
    'label_forgot' => 'Забыли пароль?',
    'label_register' => 'Регистрация',
    'label_name' => 'Введите ФИО',
    'register' => 'Зарегистрироваться',
    'label_reset_password' => 'Восстановление пароля',
    'send_link_password' => 'Восстановить',
];
