@extends('layouts.app')

@section('content')
    @include('components.searchbar')
    <div class="py-3">
        <div class="container-fluid pb-5" style="
            background-repeat: no-repeat;
            background-color: #EBF7FF;
            background-position: left;
            min-height: 20vw;
            ">
            {!! $about->content_page_front !!}
        </div>
    </div>

    @include('components.blocks.clients')
    @include('components.blocks.stocks')
    @include('components.blocks.news')
@endsection
