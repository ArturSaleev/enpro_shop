@extends('layouts.admin')

@section('content')
    @php
        $langs = \App\Helpers\SysConfig::AllLanguage();
    @endphp
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="alert alert-danger error_required_alert fade in" role="alert" style="position: fixed; z-index: 999;left:50%;transform: translateX(-50%);">
            Обьязательные поля не заполнены!
        </div>
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Аттрибуты", "title_child" => "Создать", "icon" => "fa-receipt"])
            <form method="post" action="{{ route('admin.attributes.store') }}"  onsubmit="return formValidate()" novalidate>
                @csrf
                <div class="card-body card-body-cascade">

                    <div class="classic-tabs">
                        <div class="tabs-wrapper">
                            <ul class="nav tabs-blue" role="tablist">
                                @foreach($langs as $i=>$lang)
                                    <li class="nav-item">
                                        <a
                                            class="nav-link waves-light waves-effect waves-light {{ ($i == 0) ? 'active' : '' }}"
                                            data-toggle="tab"
                                            href="#tab_{{ $lang->id }}"
                                            role="tab"
                                            aria-selected="true">
                                            {{ $lang->name }} ({{ $lang->code }})
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="tab-content card">
                            @foreach($langs as $i=>$lang)
                                <div class="tab-pane fade {{ ($i == 0) ? 'active show' : '' }}" id="tab_{{ $lang->id }}" role="tabpanel">
                                    <div class="form-group row">
                                        <label class="col-md-4">Наименование ({{ $lang->code }})</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control needs-validation" name="{{ $lang->code }}[name]" value="" required>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4">Описание ({{ $lang->code }})</label>
                                        <div class="col-md-8">
                                            <textarea class="form-control needs-validation" name="{{ $lang->code }}[content]"></textarea>
                                        </div>
                                    </div>

                                    <h5>Выпадающий список для установки параметров аттрибута</h5>

                                    <div class="form-group row">
                                        <label class="col-md-4">Параметры</label>
                                        <div class="col-md-8" id="params_{{ $lang->code }}">
                                            <div class="input-group mb-3" id="">
                                                <input type="text" class="form-control needs-validation" name="params[{{ $lang->code }}][]" value="">
                                                <span class="input-group-text btn-primary add_params" data="{{ $lang->code }}"><i class="fas fa-plus"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4"></label>
                        <div class="col-md-8">
                            <fieldset class="form-check mt-4">
                                <input class="form-check-input filled-in" name="on_view" type="checkbox" id="checkbox2" checked>
                                <label class="form-check-label" for="checkbox2">Отображаемый аттрибут в продукциях</label>
                            </fieldset>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-md-6"></label>
                        <div class="col-md-6">
                            <button class="btn btn-success btn-block"><i class="fas fa-save"></i> Сохранить</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@push('js')
    <script>
        let lang = JSON.parse('{!!  json_encode($langs) !!}');
        $('.add_params').click(function(){
            let id = $(this).attr('data');
            let ids = Math.round(Math.random() * 100000);
            $.each(lang, function(i, e){
                let html = '<div class="input-group mb-3 '+ids+'">\n' +
                    '<input type="text" class="form-control needs-validation" name="params['+e.code+'][]" placeholder="Параметр ('+e.code+')">\n' +
                    '<span class="input-group-text btn-danger del_params" data="'+ids+'"><i class="fas fa-trash"></i></span>\n' +
                    '</div>';

                $('#params_'+e.code).append(html);
            })
        })

        $('body').on('click', '.del_params', function(){
            let id = $(this).attr('data');
            $('.'+id).remove();
        })
    </script>
@endpush
