@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Каталог", "title_child" => "Аттрибуты", "icon" => "fa-receipt"])

            <div class="card-body card-body-cascade">
                @include('admin.components.table',
                ["thead" => [ "Наименование", "Показать", 'Параметры'],
                 "data" => $data,
                 "cols" => ["name", "on_view", "params"],
                 "cols_html" => ["", '<fieldset class="form-check disabled">
                                    <input class="form-check-input filled-in" type="checkbox" value=":params">
                                    <label class="form-check-label" for="checkbox2"></label>
                                </fieldset>', ""]
                ]
                )
            </div>
        </div>
    </div>
@endsection
