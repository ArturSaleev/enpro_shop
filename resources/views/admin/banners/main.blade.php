@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Каталог", "title_child" => "Акции", "icon" => "fa-folder-open"])

            <div class="card-body card-body-cascade">
                @include('admin.components.table',
                ["thead" => [ "Заголовок", "Картинка","Активный"],
                 "data" => $data,
                 "cols" => ["title", "image","active"],
                 "cols_html" => ["", '<img src=":params" style="max-width: 100px;max-height: 50px">','<fieldset class="form-check disabled">
                                    <input class="form-check-input filled-in" type="checkbox" value=":params">
                                    <label class="form-check-label" for="checkbox2"></label>
                                </fieldset>']
                ]
                )
            </div>
        </div>
    </div>
@endsection
