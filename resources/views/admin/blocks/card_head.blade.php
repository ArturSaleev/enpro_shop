<div class="admin-up d-flex justify-content-start">
    <i class="fas {{ $icon ?? '' }} primary-color py-4 mr-3 z-depth-2"></i>
    <div class="data">
        <h5 class="font-weight-bold dark-grey-text">
            {{ $title }} - <span class="text-muted">{{ $title_child ?? '' }}</span></h5>
    </div>
</div>
