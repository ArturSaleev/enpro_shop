@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Каталог", "title_child" => "Бренды", "icon" => "fa-copyright"])

            <div class="card-body card-body-cascade">
                @include('admin.components.table',
                ["thead" => [ "Наименование", "Логотип"],
                 "data" => $data,
                 "cols" => ["name", "image"],
                 "cols_html" => ["", '<img src=":params" style="max-width: 100px">']
                ]
                )
            </div>
        </div>
    </div>
@endsection
