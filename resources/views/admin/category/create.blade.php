@extends('layouts.admin')

@section('content')
    @php
        $langs = \App\Helpers\SysConfig::AllLanguage();
    @endphp

    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="alert alert-danger error_required_alert fade in" role="alert" style="position: fixed; z-index: 999;left:50%;transform: translateX(-50%);">
            Обьязательные поля не заполнены!
        </div>
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Категории", "title_child" => "Создать", "icon" => "fa-folder-open"])
            <form method="post" action="{{ route('admin.categories.store') }}"  enctype="multipart/form-data" onsubmit="return formValidate()" novalidate>
                @csrf
                <div class="card-body card-body-cascade">
                    <div class="form-group row">
                        <label for="formFile" class="col-md-4">Картинка</label>
                        <div class="col-md-8">
                            <div class="col-mb-3">
                                <input class="form-control needs-validation" type="file" id="formFile" name="image_cat">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="select_parent" class="col-md-4">Головной</label>
                        <div class="col-md-8">
                            <select name="parent_id" id="select_parent" class="form-control form-select" aria-label="Default select example">
                                <option value="0">Выберите</option>
                                @foreach ($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="classic-tabs">
                        <div class="tabs-wrapper">
                            <ul class="nav tabs-blue" role="tablist">
                                @foreach($langs as $i=>$lang)
                                    <li class="nav-item">
                                        <a
                                            class="nav-link waves-light waves-effect waves-light {{ ($i == 0) ? 'active' : '' }}"
                                            data-toggle="tab"
                                            href="#tab_{{ $lang->id }}"
                                            role="tab"
                                            aria-selected="true">
                                            {{ $lang->name }} ({{ $lang->code }})
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="tab-content card">
                            @foreach($langs as $i=>$lang)
                                <div class="tab-pane fade {{ ($i == 0) ? 'active show' : '' }}" id="tab_{{ $lang->id }}" role="tabpanel">

                                    <div class="form-group row">
                                        <label class="col-md-4">Наименование ({{ $lang->code }})</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control needs-validation" name="{{ $lang->code }}[name]" value="" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Описание категории ({{ $lang->code }})</label>
                                        @include('admin.components.html_editor', ["name" => "$lang->code[content]"])
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <hr>
                    <div class="form-group row">
                        <label class="col-md-6"></label>
                        <div class="col-md-6">
                            <button class="btn btn-success btn-block"><i class="fas fa-save"></i> Сохранить</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
