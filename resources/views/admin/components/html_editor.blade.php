<textarea class="form-control editor needs-validation" name="{{ $name }}" @if(isset($required)) required @endif>
    @if(isset($value)) {{ $value }} @endif
</textarea>

@once
@push('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.11/ckeditor.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.11/adapters/jquery.js"></script>
    <script>
        let route_prefix = '/filemanager';
        $('.editor').ckeditor({
            height: 200,
            filebrowserImageBrowseUrl: route_prefix + '?type=Images',
            filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token=mmvk9UbBvvlRuuVgPieg4m1cejKoct5yes7ZEu5P',
            filebrowserBrowseUrl: route_prefix + '?type=Files',
            filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token=mmvk9UbBvvlRuuVgPieg4m1cejKoct5yes7ZEu5P'
        });
    </script>
@endpush
@endonce
