<div class="input-group">
   <span class="input-group-btn">
     <a id="{{ $id ?? 'lfm' }}" data-input="thumbnail{{ $id ?? ''}}" data-preview="holder{{ $id ?? ''}}" class="btn btn-primary btn-sm">
       <i class="fa fa-picture-o"></i> Выберите картинку
     </a>
   </span>
    <input id="thumbnail{{ $id ?? ''}}" class="form-control needs-validation" type="text" name="{{ $name ??  'filepath' }}"
           value="@if(isset($value)) {{ $value }} @endif">
</div>
<br>
<img id="holder{{ $id ?? ''}}" style="margin-top:15px;max-height:100px;">

@push('js')
<script>
    $('#{{ $id ?? 'lfm'}}').filemanager('image');
    $('#thumbnail{{ $id ?? ''}}').change(function(){
        let val = $(this).val();
        $('#holder{{ $id ?? ''}}').attr('src', val);
    })

    $('#thumbnail{{ $id ?? ''}}').change();
</script>
@endpush
