<table class="table table-bordered">
    <thead>
    <tr>
        @foreach($thead as $th)
            <th>{{ $th }}</th>
        @endforeach
        <th>
            <a href="{{ \Illuminate\Support\Facades\URL::current() }}/create" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Создать</a>
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $arr)
        <tr>
            @foreach($cols as $i=>$col)
            <td>
                @if(!isset($cols_html[$i]))
                    {{ $arr[$col] }}
                @else
                    @php
                      if($cols_html[$i] == ''){
                        echo $arr[$col];
                      }else{
                        echo str_replace(':params', $arr[$col], $cols_html[$i]);
                        }
                    @endphp</td>
                @endif
            @endforeach
            <td>

                <div class="dropdown">
                    <a class="btn btn-default btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-cogs"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a href="{{ \Illuminate\Support\Facades\URL::current() }}/{{ $arr['id'] }}/edit" class="dropdown-item"><i class="fas fa-edit"></i> Редактировать</a>
                        <form method="post" action="{{ \Illuminate\Support\Facades\URL::current() }}/{{ $arr['id'] }}">
                            @csrf
                            {{ method_field('DELETE') }}
                            <button type="submit" class="dropdown-item"><i class="fas fa-trash"></i> Удалить</button>
                        </form>
                    </div>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="row">
    <div class="col-lg-4 offset-8">
        {{ $data->links('pagination::bootstrap-4') }}
    </div>
</div>
@once
    @push('js')
    <script>
        $(function(){
            $('input[type="checkbox"]').each(function(){
                if(this.value == '1')
                    $(this).prop('checked', true)
            })
        })
    </script>
    @endpush
@endonce
