@extends('layouts.admin')

@section('content')
    @php
        $langs = \App\Helpers\SysConfig::AllLanguage();
    @endphp
    <div class="col-md-12">
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Конфигурация", "title_child" => "Настройки", "icon" => "fa-cog"])
            <form method="post" action="{{ route('admin.config.store') }}"  enctype="multipart/form-data">
                @csrf
                <div class="card-body card-body-cascade">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>Основные параметры</h2>
                            <hr />
                            <div class="form-group row">
                                <label class="col-md-3">Логотип</label>
                                <div class="col-md-7">
                                    <input class="form-control needs-validation" type="file" id="formFile" name="logo">
                                </div>
                                <div class="col-md-2">
                                    <div class="mb-12">
                                        @isset($data->logo)
                                        <img src="{{ asset($data->logo)}}" class="img-fluid" style="max-height: 50px;">
                                        <input type="hidden" name="logo_bk" value="{{ $data->logo }}">
                                        @endempty
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3">Иконка</label>
                                <div class="col-md-7">
                                    <input class="form-control needs-validation" type="file" id="formFile" name="icon">
                                </div>
                                <div class="col-md-2">
                                    <div class="mb-12">
                                        @isset($data->icon)
                                            <img src="{{ asset($data->icon)}}" class="img-fluid" style="max-height: 50px;">
                                            <input type="hidden" name="icon_bk" value="{{ $data->icon }}">
                                        @endempty
                                    </div>
                                </div>
                            </div>

                            @foreach($langs as $lang)
                                @if(count($addr[$lang->code] ?? $addr) > 0)

                                        <div class="form-group row">
                                            <label class="col-md-3">Адрес ({{ $lang->code }})</label>
                                            <div class="col-md-9" id="address_{{ $lang->code }}">
                                                @foreach($addr[$lang->code] as $i)
                                                    @php
                                                        $ids = rand(0, 10000);
                                                    @endphp
                                                    @if ($loop->first)
                                                        <div class="input-group mb-3">
                                                            <input type="text" class="form-control" name="address[{{$lang->code}}][]" value="{{ $i->address }}">
                                                            <span class="input-group-text btn-primary address" data="{{ $lang->code }}"><i class="fas fa-plus"></i></span>
                                                        </div>
                                                    @else
                                                        <div class="input-group mb-3" id="{{ $ids }}">
                                                            <input type="text" class="form-control" name="address[{{$lang->code}}][]" value="{{ $i->address }}">
                                                            <span class="input-group-text btn-danger address" onclick="deleteAdress({{ $ids }})"><i class="fas fa-trash"></i></span>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                @endif
                            @endforeach

                            <div class="form-group row">
                                <label class="col-md-3">E-mail</label>
                                <div class="col-md-9">
                                    <input type="email" class="form-control" name="email" value="{{ $data->email ?? '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3">Номер телефона</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="phone" value="{{ $data->phone ?? '' }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h2>Страница "О НАС"</h2>
                            <hr />
                            <div class="form-group row">
                                <label for="formFile" class="col-md-4">Картинка</label>
                                <div class="col-md-8">
                                    <div class="mb-3">
                                        <input class="form-control needs-validation" type="file" id="formFile" name="image_about">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="mb-12">
                                        @isset($about->image)
                                        <img src="{{ asset($about->image)}}" class="img-fluid" style="max-height: 400px;">
                                            <input type="hidden" name="about_img_bk" value="{{ $about->image }}">
                                        @endempty
                                    </div>
                                </div>
                            </div>

                            <div class="classic-tabs">
                                <div class="tabs-wrapper">
                                    <ul class="nav tabs-blue" role="tablist">
                                        @foreach($langs as $i=>$lang)
                                            <li class="nav-item">
                                                <a
                                                    class="nav-link waves-light waves-effect waves-light {{ ($i == 0) ? 'active' : '' }}"
                                                    data-toggle="tab"
                                                    href="#tab_{{ $lang->id }}"
                                                    role="tab"
                                                    aria-selected="true">
                                                    {{ $lang->name }} ({{ $lang->code }})
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="tab-content card">
                                    @foreach($langs as $i=>$lang)
                                        <div class="tab-pane fade {{ ($i == 0) ? 'active show' : '' }}" id="tab_{{ $lang->id }}" role="tabpanel">
                                            <div class="form-group">
                                                <label>Описание ({{ $lang->code }})</label>
                                                @include('admin.components.html_editor', ["name" => "about[translations][$lang->code][content_main]", "value" => $about ? $about->translate($lang->code)->content_main : ''])
                                            </div>
                                            <div class="form-group">
                                                <label>Описание ({{ $lang->code }})</label>
                                                @include('admin.components.html_editor', ["name" => "about[translations][$lang->code][content_page]", "value" => $about ? $about->translate($lang->code)->content_page : ''])
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Сохранить</button>
                </div>
            </form>
        </div>

    </div>
@endsection
