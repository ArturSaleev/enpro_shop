@extends('layouts.admin')

@section('content')

    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Валюта", "title_child" => "Создать", "icon" => "fa-dollar-sign"])
            <form method="post" action="{{ route('admin.currency.store') }}">
                @csrf
                <div class="card-body card-body-cascade">
                    <div class="form-group row">
                        <label class="col-md-3">Код</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="code" value="" list="code_currency">
                            <datalist id="code_currency">
                                @foreach($data_currency as $dc)
                                    <option value="{{ $dc['code'] }}">{{ $dc['name'] }}</option>
                                @endforeach
                            </datalist>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3">Наименование</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="name" value="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3">Символ</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="symbol" value="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3">Курс</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="rate" value="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3">Позиция</label>
                        <div class="col-md-9">
                            <select class="form-control" name="position_left" style="display: block">
                                <option value="true">Слева</option>
                                <option value="false">Справа</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3"></label>
                        <div class="col-md-9">
                            <fieldset class="form-check mt-4">
                                <input class="form-check-input filled-in" name="on_default" type="checkbox" id="checkbox2">
                                <label class="form-check-label" for="checkbox2">Выбрать как основной</label>
                            </fieldset>
                        </div>
                    </div>


                    <hr>
                    <div class="form-group row">
                        <label class="col-md-3"></label>
                        <div class="col-md-9">
                            <button class="btn btn-success btn-block"><i class="fas fa-save"></i> Сохранить</button>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3"></label>
                        <div class="col-md-9">
                            <label class="text-info">
                                Курс валют взят с сайта {{ env('CURRENCY_SERVICE') }}
                            </label>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script>
        let cr = JSON.parse('{!! $data_currency_json !!}');
        console.log(cr);
        $("input[name=code]").on('input', function () {
            var val = this.value;
            $.each(cr, function(i, e){
                if(e.code === val){
                    $('input[name=name]').val(e.name);
                    $('input[name=rate]').val(e.rate);
                }
            })
        });
    </script>
@endpush
