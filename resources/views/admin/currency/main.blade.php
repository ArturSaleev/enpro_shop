@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Локализация", "title_child" => "Валюта", "icon" => "fa-dollar-sign"])

            <div class="card-body card-body-cascade">
                @include('admin.components.table',
                ["thead" => [ "Наименование", "Код", "Символ", "Курс", "Позиция", "Основной"],
                 "data" => $data,
                 "cols" => ["name", "code", "symbol", "rate", "position_left", "on_default"],
                 "cols_html" => ["", "", '', '', '', '']
                ]
                )
            </div>

            <div class="card-footer text-right">
                <a href="{{ route('admin.convert_currency') }}" class="btn btn-info btn-sm">Обновить курс валют</a>
            </div>
        </div>
    </div>
@endsection
