@extends('layouts.admin')

@section('content')
    <h2
        class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200 col-12"
    >
        Главная
    </h2>
    <div class="col-12 pt-3 row show_limited">
        @foreach($counts as $count)
            <div class="col-4 mb-3">
            <div class="card widget-flat">
                <div class="card-body d-flex justify-content-between">
                    <div>
                        <h5 class="text-muted fw-normal mt-0" title="{{ $count['title'] }}">{{ $count['title'] }}</h5>
                        <h3 class="mt-3 mb-3">{{ $count['data'] }}</h3>
                    </div>
                    <div class="d-flex flex-column align-items-end">
                        <div class="badge badge-primary p-2">
                            <i class="{{ $count['icon'] }} fa-2x"></i>
                        </div>
                        <a href="{{ asset($count['link']) }}" class="btn btn-outline-primary mr-0">Посмотреть</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <div class="col-12 d-flex justify-content-center">
            <button class="btn btn-primary" data-toggle="collapse" data-target="#collapseCard">Посмотреть больше</button>
        </div>
    </div>
    <div class="col-12 pt-3 row">
        <div class="col-6">
            <div class="card">
                <h4 class="m-0 pt-2 pl-2">Последние 5 выполненных заказов</h4>
                <hr class="my-2">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Наименование</th>
                        <th scope="col">Продукт</th>
                        <th scope="col">Количество</th>
                        <th scope="col">Дата</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($recentServedOrders) > 0)
                    @foreach($recentServedOrders as $order)
                        <tr>
                            <th scope="row">{{ $order->product->name }}</th>
                            <td>{{ $order->product->sku }}</td>
                            <td>{{ $order->quantity }}</td>
                            <td>{{ $order->created_at }}</td>
                        </tr>
                    @endforeach
                    @else
                        <tr>
                            <td colspan="5" class="text-center">
                                Нет выполненных заказов
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <a href="{{ route('admin.order.index') }}" class="btn btn-outline-primary mr-0">Посмотреть</a>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <h4 class="m-0 pt-2 pl-2">Последние 5 не выполненных заказов</h4>
                <hr class="my-2">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Наименование</th>
                        <th scope="col">Продукт</th>
                        <th scope="col">Количество</th>
                        <th scope="col">Дата</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($recentUnservedOrders) > 0)
                    @foreach($recentUnservedOrders as $order)
                    <tr>
                        <th scope="row">{{ $order->product->name }}</th>
                        <td>{{ $order->product->sku }}</td>
                        <td>{{ $order->quantity }}</td>
                        <td>{{ $order->created_at }}</td>
                    </tr>
                    @endforeach
                    @else
                        <tr>
                            <td colspan="5" class="text-center">
                                Нет не выполненных заказов
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <a href="{{ route('admin.order.index') }}" class="btn btn-outline-primary mr-0">Посмотреть</a>
            </div>
        </div>
    </div>
@endsection
