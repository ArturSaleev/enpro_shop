@extends('layouts.admin')

@section('content')
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Языки", "title_child" => "Редактирование", "icon" => "fa-language"])
            <form method="post" action="{{ route('admin.language.update', $data->id) }}">
                @method('put')
                @csrf
                <div class="card-body card-body-cascade">
                    <div class="form-group row">
                        <label class="col-md-6">Наименование</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="name" value="{{ $data->name ?? '' }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-6">Код</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="code" value="{{ $data->code ?? '' }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-6">Логотип</label>
                        <div class="col-md-6">
                            @include('admin.components.input_file', ["name" => "image", "value" => $data->image ?? ''])
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-6"></label>
                        <div class="col-md-6">
                            <fieldset class="form-check mt-4">
                                <input class="form-check-input filled-in" name="on_default" type="checkbox" id="checkbox2"
                                @if(isset($data->on_default))
                                    {{ ((bool)$data->on_default) ? 'checked="checked"' : ''}}
                                @endif
                                >
                                <label class="form-check-label" for="checkbox2">Выбрать как основной</label>
                            </fieldset>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-md-6"></label>
                        <div class="col-md-6">
                            <button class="btn btn-success btn-block"><i class="fas fa-save"></i> Сохранить</button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
@endsection
