@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Локализация", "title_child" => "Языки", "icon" => "fa-language"])

            <div class="card-body card-body-cascade">
                @include('admin.components.table',
                ["thead" => [ "Наименование", "Код", "Логотип", "Основной"],
                 "data" => $data,
                 "cols" => ["name", "code", "image", "on_default"],
                 "cols_html" => ["", "", '<img src=":params">', '<fieldset class="form-check disabled">
                                    <input class="form-check-input filled-in" type="checkbox" value=":params">
                                    <label class="form-check-label" for="checkbox2"></label>
                                </fieldset>']
                ]
                )
            </div>
        </div>
    </div>
@endsection
