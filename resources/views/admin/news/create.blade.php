@extends('layouts.admin')

@section('content')
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="card card-cascade cascading-admin-card user-card">
            @php
                $langs = \App\Helpers\SysConfig::AllLanguage();
            @endphp
            @include('admin.blocks.card_head', ["title" => "Новости", "title_child" => "Создать", "icon" => "fa-newspaper"])
            <form method="post" action="{{ route('admin.news.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body card-body-cascade">
                    <div class="form-group row">
                        <label for="formFile" class="col-md-4">Картинка</label>
                        <div class="col-md-8">
                            <div class="col-mb-3">
                                <input class="form-control" type="file" id="formFile" name="image_news">
                            </div>
                        </div>
                    </div>
                    <div class="classic-tabs">
                        <div class="tabs-wrapper">
                            <ul class="nav tabs-blue" role="tablist">
                                @foreach($langs as $i=>$lang)
                                    <li class="nav-item">
                                        <a
                                            class="nav-link waves-light waves-effect waves-light {{ ($i == 0) ? 'active' : '' }}"
                                            data-toggle="tab"
                                            href="#tab_{{ $lang->id }}"
                                            role="tab"
                                            aria-selected="true">
                                            {{ $lang->name }} ({{ $lang->code }})
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="tab-content card">
                            @foreach($langs as $i=>$lang)
                                <div class="tab-pane fade {{ ($i == 0) ? 'active show' : '' }}" id="tab_{{ $lang->id }}" role="tabpanel">
                                    <div class="form-group">
                                        <label>Заголовок ({{ $lang->code }})</label>
                                        <input type="text" class="form-control" name="{{ $lang->code }}[title]" value="" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Текст ({{ $lang->code }})</label>
                                        @include('admin.components.html_editor', ["name" => "$lang->code[content]", "required"])
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <hr>
                    <div class="form-group row">
                        <label class="col-md-6"></label>
                        <div class="col-md-6">
                            <button class="btn btn-success btn-block"><i class="fas fa-save"></i> Сохранить</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
