@extends('layouts.admin')

@section('content')
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Языки", "title_child" => "Создать", "icon" => "fa-language"])
            <form method="post" action="{{ route('admin.order.store') }}">
                @csrf
                <div class="card-body card-body-cascade">

                    <div class="form-group row">
                        <label class="col-md-3">Продукты</label>
                        <div class="col-md-9">
                            <select class="form-control" name="product_id" style="display: block">
                                @foreach($data as $dc)
                                    <option value="{{ $dc->id }}">{{ $dc->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-6">Версия</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="version" value="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-6">Кол-во</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="quantity" value="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-6">подробности</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="details" value="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-6">Связаться</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="contact" value="">
                        </div>
                    </div>

                    <hr>
                    <div class="form-group row">
                        <label class="col-md-6"></label>
                        <div class="col-md-6">
                            <button class="btn btn-success btn-block"><i class="fas fa-save"></i> Сохранить</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
