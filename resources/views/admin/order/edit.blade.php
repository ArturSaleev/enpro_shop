@extends('layouts.admin')

@section('content')
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Заказать", "title_child" => "Заказать", "icon" => "fas fa-file-invoice-dollar"])
            <form method="post" action="{{ route('admin.order.update', $data->id) }}">
                @method('put')
                @csrf
                <div class="card-body card-body-cascade">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <fieldset class="form-check mt-4">
                                <input class="form-check-input filled-in" name="served[1]" type="checkbox" id="checkbox2" value="{{$data->served}}"
                                @if(isset($data->served))
                                    {{ ((bool)$data->served) ? 'checked="checked"' : ''}}
                                @endif
                                >
                                <label class="form-check-label" for="checkbox2">сделано?</label>
                                <input type="hidden" name="served[0]" value="0" />
                            </fieldset>
                        </div>
                        <label class="col-md-6"></label>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-md-6"></label>
                        <div class="col-md-6">
                            <button class="btn btn-success btn-block"><i class="fas fa-save"></i> Сохранить</button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
@endsection
