@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Заказать", "title_child" => "Заказать", "icon" => "fas fa-file-invoice-dollar"])

            <div class="card-body card-body-cascade">
                @include('admin.components.table',
                ["thead" => [ "Версия", "Кол-во", "подробности", "Связаться", "Сделано"],
                 "data" => $data,
                 "cols" => ["version", "quantity", "details", "contact", "served"],
                 "cols_html" => ["", "", '', '', '<fieldset class="form-check disabled">
                                    <input class="form-check-input filled-in" type="checkbox" value=":params">
                                    <label class="form-check-label" for="checkbox2"></label>
                                </fieldset>']
                ]
                )
            </div>
        </div>
    </div>
@endsection
