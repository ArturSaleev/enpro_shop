@extends('layouts.admin')

@section('content')
    @php
        $langs = \App\Helpers\SysConfig::AllLanguage();
    @endphp
    <form method="post" action="{{ route('admin.products.store') }}" style="width: 100%" enctype="multipart/form-data" onsubmit="return formValidate()" novalidate>
        @csrf
        <div class="alert alert-danger error_required_alert fade in" role="alert" style="position: fixed; z-index: 999;left:50%;transform: translateX(-50%);">
            Обьязательные поля не заполнены!
        </div>
    <div class="col-md-12">
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Каталог", "title_child" => "Продукты", "icon" => "fa-file-image"])

                <div class="card-body card-body-cascade">

                    <div class="classic-tabs">
                        <div class="tabs-wrapper">
                            <ul class="nav tabs-blue" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link waves-light waves-effect waves-light active"
                                       data-toggle="tab"
                                       href="#tab_main"
                                       role="tab" aria-selected="true">
                                        Основные
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link waves-light waves-effect waves-light"
                                       data-toggle="tab"
                                       href="#tab_dan"
                                       role="tab" aria-selected="true">
                                        Данные
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link waves-light waves-effect waves-light"
                                       data-toggle="tab"
                                       href="#tab_link"
                                       role="tab" aria-selected="true">
                                        Связи
                                    </a>
                                </li>
                                {{--<li class="nav-item">
                                    <a class="nav-link waves-light waves-effect waves-light"
                                       data-toggle="tab"
                                       href="#tab_attribut"
                                       role="tab" aria-selected="true">
                                        Аттрибуты
                                    </a>
                                </li>--}}
                                <li class="nav-item">
                                    <a class="nav-link waves-light waves-effect waves-light"
                                       data-toggle="tab"
                                       href="#tab_files"
                                       role="tab" aria-selected="true">
                                        Файлы
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content card">
                            <div class="tab-pane fade active show" id="tab_main" role="tabpanel">
                                @include('admin.product.create_tabs.main')
                            </div>
                            <div class="tab-pane fade" id="tab_dan" role="tabpanel">
                                @include('admin.product.create_tabs.dan')
                            </div>
                            <div class="tab-pane fade" id="tab_link" role="tabpanel">
                                @include('admin.product.create_tabs.link')
                            </div>
                            {{--<div class="tab-pane fade" id="tab_attribut" role="tabpanel">
                                @include('admin.product.create_tabs.attribut')
                            </div>--}}
                            <div class="tab-pane fade" id="tab_files" role="tabpanel">
                                @include('admin.product.create_tabs.files', ['imageSections' => $imageSections])
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="form-group row">
                        <label class="col-md-6"></label>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-success btn-block"><i class="fas fa-save"></i> Сохранить</button>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </form>
@endsection
