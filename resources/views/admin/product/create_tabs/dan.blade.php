<div class="form-group row">
    <label class="col-md-3">Артикуль / SKU</label>
    <div class="col-md-9">
        <input type="text" class="form-control needs-validation" name="sku" value="" required>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3">Цена</label>
    <div class="col-md-9">
        <input type="number" class="form-control needs-validation" name="cost" value="" required>
    </div>
</div>


<div class="form-group row">
    <label class="col-md-3">Размеры</label>
    <div class="col-md-3">
        <input type="number" class="form-control needs-validation" name="length" value="" placeholder="Длина">
    </div>
    <div class="col-md-3">
        <input type="number" class="form-control needs-validation" name="width" value="" placeholder="Ширина">
    </div>
    <div class="col-md-3">
        <input type="number" class="form-control needs-validation" name="height" value="" placeholder="Высота">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3">Единица измерения длины</label>
    <div class="col-md-9">
        <select class="form-control" name="length_id">
            @foreach((new \App\Helpers\Referency())->ref_length() as $ln)
            <option value="{{ $ln->id }}">{{ $ln->name }} ({{ $ln->abbreviate }})</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3">Единица измерения веса</label>
    <div class="col-md-3">
        <input type="number" class="form-control needs-validation" name="weight" value="" placeholder="Вес">
    </div>
    <div class="col-md-6">
        <select class="form-control" name="weight_id">
            @foreach((new \App\Helpers\Referency())->ref_weight() as $rw)
                <option value="{{ $rw->id }}">{{ $rw->name }} ({{ $rw->abbreviate }})</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3">Статус</label>
    <div class="col-md-9">
        <fieldset>
            <input class="form-check-input filled-in" name="status" value="1" type="checkbox" id="checkbox2">
            <label class="form-check-label" for="checkbox2">Активный</label>
        </fieldset>
    </div>
</div>
