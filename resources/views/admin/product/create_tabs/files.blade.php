@foreach($imageSections as $key => $value)
{{ $value }}
<div class="row">
{{--          <span class="input-group-btn">--}}
{{--            <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary btn-sm text-white">--}}
{{--              <i class="fas fa-picture-o"></i> Choose--}}
{{--            </a>--}}
{{--          </span>--}}
    <div class="col-6">
        <input id="thumbnail" class="form-control needs-validation" type="file" name="{{ $value }}">
    </div>
</div>
<br>
{{--<div id="holder2" class="row" style="margin-top:15px;max-height:100px;">--}}
{{--    <div class="col-md-3 row text-center">--}}
{{--        <div class="col-md-4">--}}
{{--            <i class="fas fa-3x fa-file-pdf text-danger"></i>--}}
{{--        </div>--}}
{{--        <div class="col-md-8">--}}
{{--            <input type="text" class="form-control" name="ru[file_text][1]" value="" placeholder="Название файла (ru)">--}}
{{--            <input type="text" class="form-control" name="en[file_text][1]" value="" placeholder="Название файла (en)">--}}
{{--            <input type="hidden" name="file[{{ $key }}][path]" value="">--}}
{{--            <input type="hidden" name="file[{{ $key }}][section]" value="{{ $value }}">--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
@endforeach
@push('js')
{{--    <script>--}}
{{--        var lfm = function(id, type, options) {--}}
{{--            let button = document.getElementById(id);--}}

{{--            button.addEventListener('click', function () {--}}
{{--                var route_prefix = (options && options.prefix) ? options.prefix : '/filemanager';--}}
{{--                var target_input = document.getElementById(button.getAttribute('data-input'));--}}
{{--                let prev_id = button.getAttribute('data-preview');--}}
{{--                var target_preview = document.getElementById(prev_id);--}}

{{--                window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');--}}
{{--                window.SetUrl = function (items) {--}}
{{--                    var file_path = items.map(function (item) {--}}
{{--                        return item.url;--}}
{{--                    }).join(',');--}}

{{--                    // set the value of the desired input to image url--}}
{{--                    target_input.value = file_path;--}}
{{--                    target_input.dispatchEvent(new Event('change'));--}}

{{--                    // clear previous preview--}}
{{--                    //target_preview.innerHtml = '';--}}

{{--                    // set or change the preview image src--}}

{{--                    items.forEach(function (item) {--}}
{{--                        let html = '<div class="col-md-3 row text-center">\n' +--}}
{{--                            '        <div class="col-md-4">\n' +--}}
{{--                            '            <i class="fas fa-3x fa-file-'+item.icon+' text-danger"></i>\n' +--}}
{{--                            '        </div>\n' +--}}
{{--                            '        <div class="col-md-8">\n' +--}}
{{--                            '            <input type="text" class="form-control" name="ru[file_text][1]" value="'+item.name+'" placeholder="Название файла (ru)">\n' +--}}
{{--                            '            <input type="text" class="form-control" name="en[file_text][1]" value="'+item.name+'" placeholder="Название файла (en)">\n' +--}}
{{--                            '            <input type="hidden" name="file[1][path]" value="'+item.url+'">\n' +--}}
{{--                            '        </div>\n' +--}}
{{--                            '    </div>';--}}

{{--                        $('#'+prev_id).append(html);--}}

{{--                        //target_preview.append(html);--}}
{{--                        //console.log(item);--}}
{{--                        /*--}}
{{--                        let img = document.createElement('img')--}}
{{--                        img.setAttribute('style', 'height: 5rem')--}}
{{--                        img.setAttribute('src', item.thumb_url)--}}
{{--                        target_preview.appendChild(img);--}}
{{--                        */--}}
{{--                    });--}}

{{--                    // trigger change event--}}
{{--                    target_preview.dispatchEvent(new Event('change'));--}}
{{--                };--}}
{{--            });--}}
{{--        };--}}

{{--        lfm('lfm', 'file', {prefix: route_prefix});--}}
{{--    </script>--}}
@endpush
