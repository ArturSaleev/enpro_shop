@foreach((new \App\Helpers\Referency())->all_attributes() as $attribute)

<div class="form-group row">
    <label class="col-md-3">{{ $attribute->name }}</label>
    <div class="col-md-9">
        <select class="form-control" name="attribute[][attribute_id]">
            @foreach($attribute->attributeParams as $param)
                <option selected="{{ array_search($param->id, array_column($data->productAttributes->toArray(), 'attribute_id')) }}" value="{{ $param->id }}">{{ $param->name }}</option>
            @endforeach
        </select>
    </div>
</div>
@endforeach
