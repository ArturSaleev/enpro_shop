<div class="form-group row">
    <label class="col-md-3">Артикуль / SKU</label>
    <div class="col-md-9">
        <input type="text" class="form-control needs-validation" name="sku" value="{{ $data->sku }}" required>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3">Цена</label>
    <div class="col-md-9">
        <input type="number" class="form-control needs-validation" name="cost" value="{{ $data->cost }}" required>
    </div>
</div>


<div class="form-group row">
    <label class="col-md-3">Размеры</label>
    <div class="col-md-3">
        <input type="number" class="form-control needs-validation" name="length" value="{{ $data->length }}" placeholder="Длина">
    </div>
    <div class="col-md-3">
        <input type="number" class="form-control needs-validation" name="width" value="{{ $data->width }}" placeholder="Ширина">
    </div>
    <div class="col-md-3">
        <input type="number" class="form-control needs-validation" name="height" value="{{ $data->height }}" placeholder="Высота">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3">Единица измерения длины</label>
    <div class="col-md-9">
        <select class="form-control" name="length_id">
            @foreach((new \App\Helpers\Referency())->ref_length() as $ln)
            <option selected="{{ $data->length_id === $ln->id }}" value="{{ $ln->id }}">{{ $ln->name }} ({{ $ln->abbreviate }})</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3">Единица измерения веса</label>
    <div class="col-md-3">
        <input type="number" class="form-control needs-validation" name="weight" value="{{ $data->weight }}" placeholder="Вес">
    </div>
    <div class="col-md-6">
        <select class="form-control" name="weight_id">
            @foreach((new \App\Helpers\Referency())->ref_weight() as $rw)
                <option selected="{{ $data->weight_id === $rw->id }}" value="{{ $rw->id }}">{{ $rw->name }} ({{ $rw->abbreviate }})</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3">Статус</label>
    <div class="col-md-9">
        <fieldset>
            <input class="form-check-input filled-in" name="status" checked="{{ $data->status }}" type="checkbox" id="checkbox2">
            <label class="form-check-label" for="checkbox2">Активный</label>
        </fieldset>
    </div>
</div>
