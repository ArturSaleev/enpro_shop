@foreach($imageSections as $key => $value)
{{ $value }}
<div class="row">
    <div class="col-6"><input id="thumbnail2" class="form-control" type="file" name="{{ $value }}"></div>
    <div class="col-6">
        @foreach($data->productImages as $value2)
            @if ($value2->$value == 1)
                <img src="{{ asset($value2->image)}}" class="img-fluid" style="max-height: 50px;">
            @endif
        @endforeach
    </div>
</div>
<br>
@endforeach
@push('js')
@endpush
