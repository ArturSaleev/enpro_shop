<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div>
                    <a class="float-right" href="{{ route('admin.brands.index') }}" target="_blank"><i class="fas fa-link"></i></a>
                    <a class="float-right mr-3" id="reload_brand" href="#"><i class="fas fa-sync"></i></a>
                    <h5>Бренды</h5>
                </div>
                <div id="list_brands">
                @foreach((new\App\Helpers\Referency)->all_brands() as $brand)
                    <fieldset>
                        @if(is_numeric(array_search($brand->id, array_column($data->productBrands->toArray(), 'brand_id'))))

                            <input class="form-check-input filled-in" checked name="brands[][brand_id]" value="{{ $brand->id }}" type="checkbox" id="ch_brand_{{ $brand->id }}">
                        @else
                            <input class="form-check-input filled-in" name="brands[][brand_id]" value="{{ $brand->id }}" type="checkbox" id="ch_brand_{{ $brand->id }}">
                        @endif

                        <label class="form-check-label" for="ch_brand_{{ $brand->id }}">{{ $brand->name }}</label>
                    </fieldset>
                @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div>
                    <a class="float-right" href="{{ route('admin.categories.index') }}" target="_blank"><i class="fas fa-link"></i></a>
                    <a class="float-right mr-3" id="reload_category" href="#"><i class="fas fa-sync"></i></a>
                    <h5>Категории</h5>
                </div>
                <div id="list_categories">
                    @foreach((new\App\Helpers\Referency)->all_category() as $category)
                        <fieldset>
                            @if(is_numeric(array_search($category->id, array_column($data->productCategories->toArray(), 'category_id'))))
                                <input class="form-check-input filled-in" checked name="category[][category_id]" value="{{ $category->id }}" type="checkbox" id="ch_category_{{ $category->id }}">
                            @else
                                <input class="form-check-input filled-in" name="category[][category_id]" value="{{ $category->id }}" type="checkbox" id="ch_category_{{ $category->id }}">
                            @endif
                            <label class="form-check-label" for="ch_category_{{ $category->id }}">{{ $category->name }}</label>
                        </fieldset>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@push('js')
    <script>
        $('#reload_brand').click(function(e){
            e.preventDefault()

            $.get('/api/brands', function(data){
                $('#list_brands').html('');
                $.each(data, function(i, e){
                    let html = '<fieldset>\n' +
                        '<input class="form-check-input filled-in" name="brands[]" value="'+e.id+'" type="checkbox" id="ch_brand_'+e.id+'">\n' +
                        '<label class="form-check-label" for="ch_brand_'+e.id+'">'+e.name+'</label>\n' +
                        '</fieldset>';
                    $('#list_brands').append(html);
                })
            })
        })

        $('#reload_category').click(function(e){
            e.preventDefault()

            $.get('/api/categories', function(data){
                $('#list_categories').html('');
                $.each(data, function(i, e){
                    let html = '<fieldset>\n' +
                        '<input class="form-check-input filled-in" name="brands[]" value="'+e.id+'" type="checkbox" id="ch_category_'+e.id+'">\n' +
                        '<label class="form-check-label" for="ch_category_'+e.id+'">'+e.name+'</label>\n' +
                        '</fieldset>';
                    $('#list_categories').append(html);
                })
            })
        })
    </script>
@endpush
