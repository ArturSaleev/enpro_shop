@php
    $langs = \App\Helpers\SysConfig::AllLanguage();
@endphp

<div class="classic-tabs">
    <div class="tabs-wrapper">
        <ul class="nav tabs-dark" role="tablist">
            @foreach($langs as $i=>$lang)
                <li class="nav-item">
                    <a
                        class="nav-link waves-light waves-effect waves-light {{ ($i == 0) ? 'active' : '' }}"
                        data-toggle="tab"
                        href="#tab_{{ $lang->id }}"
                        role="tab"
                        aria-selected="true">
                        {{ $lang->name }} ({{ $lang->code }})
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="tab-content card">
        @foreach($langs as $i=>$lang)
            <div class="tab-pane fade {{ ($i == 0) ? 'active show' : '' }}" id="tab_{{ $lang->id }}" role="tabpanel">
                <div class="form-group">
                    <label>Наименование ({{ $lang->code }})</label>
                    <input type="text" class="form-control needs-validation" name="{{ $lang->code }}[name]" value="{{ $data->translate($lang->code)->name }}" required>
                </div>
                <div class="form-group">
                    <label>Meta ({{ $lang->code }})</label>
                    <textarea rows="6" style="resize: none" class="form-control needs-validation" name="{{ $lang->code }}[meta]" required>{{ $data->translate($lang->code)->meta }}</textarea>
                </div>
                <div class="form-group">
                    <label>Описание ({{ $lang->code }})</label>
                    @include('admin.components.html_editor', ["name" => "$lang->code[description]", "required", 'value' => $data->translate($lang->code)->description])
                </div>
                <div class="form-group">
                    <label>Мин. требования ({{ $lang->code }})</label>
                    @include('admin.components.html_editor', ["name" => "$lang->code[requirement]", "required", 'value' => $data->translate($lang->code)->requirement])
                </div>
                <div class="form-group">
                    <label>Версии ({{ $lang->code }})</label>
                    @include('admin.components.html_editor', ["name" => "$lang->code[version]", "required", 'value' => $data->translate($lang->code)->version])
                </div>
                <div class="form-group">
                    <label>Купить ({{ $lang->code }})</label>
                    @include('admin.components.html_editor', ["name" => "$lang->code[buy_description]", "required", 'value' => $data->translate($lang->code)->buy_description])
                </div>
            </div>
        @endforeach
    </div>
</div>
