@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Каталог", "title_child" => "Продукты", "icon" => "fa-file-image"])

            <div class="card-body card-body-cascade">
                @include('admin.components.table',
                ["thead" => [ "Артикуль/SKU", "Наименование", "Цена"],
                 "data" => $data,
                 "cols" => ["sku", "name", "cost"],
                 "cols_html" => ["", "", ""]
                ]
                )
            </div>
        </div>
    </div>
@endsection
