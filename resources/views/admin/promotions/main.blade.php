@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <div class="card card-cascade cascading-admin-card user-card">
            @include('admin.blocks.card_head', ["title" => "Каталог", "title_child" => "Акции", "icon" => "fa-folder-open"])

            <div class="card-body card-body-cascade">
                @include('admin.components.table',
                ["thead" => [ "Наименование", "Картинка"],
                 "data" => $data,
                 "cols" => ["title", "image"],
                 "cols_html" => ["", '<img src=":params" style="max-width: 100px;max-height: 50px">']
                ]
                )
            </div>
        </div>
    </div>
@endsection
