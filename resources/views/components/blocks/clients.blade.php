<section class="py-6">
    <div class="container">
        <div class="row align-content-center">
            <div class="col-12 col-lg-12 col-sm-12" style="text-align: center">
                <h3>{{ __('all.ourclients') }}</h3>
            </div>
            @foreach($clients as $c)
                <div class="col-6 col-lg-2 col-sm-3 clients">
                    <img src="{{ asset($c->image) }}">
                    <h4>{{$c->name}}</h4>
                </div>
            @endforeach
        </div>
    </div>
</section>
