<section
    class="section swiper-container swiper-xxl swiper-slider-1 swiper-container-initialized swiper-container-horizontal"
    data-autoplay="false" data-loop="true">
    <div class="swiper-wrapper text-center text-lg-left"
         style="transition-duration: 0ms; transform: translate3d(-1903px, 0px, 0px);">

        @foreach($products as $key => $p)
            <div class="swiper-slide swiper-slide-caption product_slider"
                 style="background-image: url('{{ asset($p->imagemain) }}');"
                 data-swiper-slide-index="{{$key++}}">
                <div class="swiper-slide-caption section-md text-center container">
                    <div class="row">
                        <div class="col-lg-6 col-6" style="text-align: left">
                            <label class="h3" style="color: #000">{{ $p->name }}</label>
                            <p class="mt-3">
                                {{ $p->meta }}
                            </p>
                            <br>
                            <a href="{{ asset("products/$p->id") }}" class="btn btn-primary">{{ __('all.details') }}</a>
                            <p>
                                          <span  class="swiper-prev-btn btn btn-primary btn-outline new_product_prev_btn sw-btn-prev" id="new_product_prev_btn" tabindex="0" role="button"
                                                 aria-label="Previous slide">
                                              <span class="fa fa-angle-left"></span>
                                          </span>
                                <span class="swiper-next-btn btn btn-primary btn-outline new_product_prev_btn sw-btn-prev" id="new_product_next_btn" tabindex="0" role="button" aria-label="Next slide">
                                              <span class="fa fa-angle-right"></span>
                                          </span>
                            </p>
                        </div>
                    </div>

                </div>
            </div>

        @endforeach

    </div>
    <!-- Swiper Navigation-->
    <div class="swiper-prev-btn" id="new_product_prev_btn" tabindex="0" role="button"
         aria-label="Previous slide"></div>
    <div class="swiper-next-btn" id="new_product_next_btn" tabindex="0" role="button" aria-label="Next slide"></div>
    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
</section>

