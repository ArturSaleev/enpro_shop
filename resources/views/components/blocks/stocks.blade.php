<div class="container-fluid py-3">
    <h3 class="text-center">{{ __('all.stocknav') }}</h3><br />
    <section
        class="section swiper-container swiper-container-initialized swiper-container-horizontal"
        data-autoplay="false" data-loop="true" style="margin-bottom: 1em">
        <div class="swiper-wrapper text-center text-lg-left" style="transition-duration: 0ms; transform: translate3d(-1903px, 0px, 0px);">
            @php
                $pos = 0
            @endphp
            @for ($i = 0; $i < count($promotions)/3; $i++)
                <div class="swiper-slide swiper-slide-caption">
                    <div class="swiper-slide-caption section-md text-center container">
                        <div class="row">
                            @for ($j = 0; $j < 3; $j++)
                                @isset($promotions[$pos])
                                    <div class="col-lg-4 col-4 col-sm-6 text-center">
                                        <p><img src="{{ asset($promotions[$pos]->image) }}" width="50%"></p>
                                        <p>{{$promotions[$pos]->title}}</p>
                                        <a href="{{ asset("promotions/".$promotions[$pos]->id) }}" class="btn btn-primary">{{ __('all.details') }}</a>
                                    </div>
                                    @php
                                        $pos++
                                    @endphp
                                @endisset
                            @endfor
                        </div>
                    </div>
                </div>
            @endfor

        </div>
        <!-- Swiper Navigation-->
        <div class="swiper-btn swiper-prev-btn btn btn-primary" tabindex="0" role="button">
            <i class="fa fa-angle-left"></i>
        </div>
        <div class="swiper-btn swiper-next-btn btn btn-primary" tabindex="0" role="button">
            <i class="fa fa-angle-right"></i>
        </div>
        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
    </section>
</div>
