<footer class="position-relative z-index-10 d-print-none">
    <!-- Main block - menus, subscribe form-->
    <div class="py-3 border-top text-muted">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-5 mb-lg-0" id="footer-contact">
                    <div class="fw-bold text-uppercase text-dark mb-3">
                        <img src="{{ asset($footer->logo) }}" width="104" height="51">
                    </div>
                    <p><a href="mailto:sales@psolution.ru">{{$footer->email}}</a></p>
                    <p><a href="#">{{$footer->phone}}</a></p>
                </div>
                <div class="col-lg-6 mb-5 mb-lg-0 align-self-center" id="footer-address">
                    <ul class="list-unstyled float-end">
                        @foreach($addresses as $address)
                            <li>{{$address}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
