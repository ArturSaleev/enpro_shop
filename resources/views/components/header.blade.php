<header class="header">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('images/logo_icon.png') }}"  width="25" height="32" style="margin-right: 15px"/>
                <span style="text-transform: uppercase; color: #4D5473; font-weight: bold">{{ config('app.name', 'PSOLUTION') }}</span>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <nav class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <div class="navbar-nav ms-auto">
                    <!-- Authentication Links -->
                    <li class="nav-item m-1">
                        <a class="nav-link" href="{{ asset('products') }}">{{ __('all.productnav') }}</a>
                    </li>
                    <li class="nav-item m-1">
                        <a class="nav-link" href="{{ asset('about') }}">{{ __('all.aboutusnav') }}</a>
                    </li>
                    <li class="nav-item m-1">
                        <a class="nav-link" href="{{ asset('news') }}">{{ __('all.newsnav') }}</a>
                    </li>
                    <li class="nav-item m-1">
                        <a class="nav-link" href="{{ asset('promotions') }}">{{ __('all.stocknav') }}</a>
                    </li>
                    <li class="nav-item m-1">
                        <a class="nav-link" href="{{ asset('products') }}"><i class="fa fa-search" aria-hidden="true"></i></a>
                    </li>

                    @guest
                        <li class="nav-item dropdown m-1" id="dropdownAuth">
                            <a class="nav-link dropdown-toggle ms-2" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-user"></i>
                            </a>
                            <ul class="dropdown-menu" id="dropdown-menuAuth" aria-labelledby="navbarDropdown">
                                @if (Route::has('login'))
                                    <li><a class="dropdown-item" href="{{ route('login') }}">{{ __('auth.Login') }}</a></li>
                                @endif

                                @if (Route::has('register'))
                                    <li><a class="dropdown-item" href="{{ route('register') }}">{{ __('auth.label_register') }}</a></li>
                                @endif
                            </ul>
                        </li>
                    @else
                        <li class="nav-item dropdown m-1" id="dropdownAuth">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle ms-2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" id="dropdown-menuAuth" aria-labelledby="navbarDropdown">
                                @if(\Illuminate\Support\Facades\Auth::user()->is_admin)
                                    <a href="{{ asset('admin/home') }}" class="dropdown-item">{{ __('all.adminpanel') }}</a>
                                @endif
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('auth.logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest

                    <li class="nav-item dropdown m-1" id="dropdownLang">
                        <a class="nav-link dropdown-toggle ms-2" href="#" role="button"  data-bs-toggle="dropdown" aria-expanded="false">
                            @switch(app()->getLocale())
                                @case('en')
                                <img
                                    src="{{ asset('images/icons/flag_en.svg') }}" alt="english flag"
                                    width="30" height="30">
                                @break

                                @case('ru')
                                <img
                                    src="{{ asset('images/icons/flag_ru.svg') }}" alt="russian flag"
                                    width="30" height="30">
                                @break

                                @default

                            @endswitch
                        </a>
                        <ul class="dropdown-menu" id="dropdown-menuLang" aria-labelledby="navbarDropdownLang">
                            <li><a class="dropdown-item" href="{{ url('lang/en') }}">English</a></li>
                            <li><a class="dropdown-item" href="{{ url('lang/ru') }}">Русский</a></li>
                        </ul>
                    </li>



                </div>
            </nav>
        </div>
    </nav>
</header>
</div>
