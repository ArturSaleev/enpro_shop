<section class="py-6 section section-sm bg-default">
    <div class="container">

        <div class="row row-30 row-lg-50">
            <div class="col-sm-6 col-md-4 col-lg-4">
                <article class="product fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                    <div class="product-body">
                        <div class="product-figure">
                            <img src="{{ asset('images/garant.png') }}" class="my-main-img">
                        </div>
                        <div class="product-price-wrap">
                            <h4>{{ __('all.homefeaturetitle1') }}</h4><br>
                            {{ __('all.homefeaturecontent1') }}
                        </div>
                    </div>
                </article>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4">
                <article class="product fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                    <div class="product-body">
                        <div class="product-figure">
                            <img src="{{ asset('images/watch.png') }}" class="my-main-img">
                        </div>
                        <div class="product-price-wrap">
                            <h4>{{ __('all.homefeaturetitle2') }}</h4><br>
                            {{ __('all.homefeaturecontent2') }}
                        </div>
                    </div>
                </article>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4">
                <article class="product fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                    <div class="product-body">
                        <div class="product-figure">
                            <img src="{{ asset('images/woucher.png') }}" class="my-main-img">
                        </div>
                        <div class="product-price-wrap">
                            <h4>{{ __('all.homefeaturetitle3') }}</h4><br>
                            {{ __('all.homefeaturecontent3') }}
                        </div>
                    </div>
                </article>
            </div>

        </div>

    </div>
</section>
