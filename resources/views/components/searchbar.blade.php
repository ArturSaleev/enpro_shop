<div class="container mt-3 mb-3">
    <div class="row justify-content-center">
        <div class="col-lg-4 col-12">
            <form class="row g-3" method="POST" action="{{ url("/products/search") }}" id="search-sku">
                <div class="col-10">
                    <input name="sku" type="text" class="form-control" placeholder="{{ __('all.searchbar') }}">
                </div>
                <div class="col-auto">
                    <button type="button" class="btn btn-primary" onclick="event.preventDefault();document.getElementById('search-sku').submit();" style="background-color: #4D5473;border-color: #4D5473;"><i class="fa fa-search"></i></button>
                </div>
                @csrf
            </form>
        </div>
    </div>
</div>
