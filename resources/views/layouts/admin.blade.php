<!DOCTYPE html>
<html :class="{ 'theme-dark': dark }" x-data="data()" lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Admin Panel</title>
    <link
        href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700;800&display=swap"
        rel="stylesheet"
    />
    <link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('admin/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/mdb.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/addons/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/addons/datatables-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">
    @stack('css')
</head>
<body class="fixed-sn white-skin">

    @include('admin.blocks.aside')
    <main>
        <div class="container-fluid">
            <section class="section team-section">
                <div class="row">
                    @yield('content')
                </div>
            </section>
        </div>
    </main>




    <script src="{{ asset('admin/js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('admin/js/popper.min.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap.js') }}"></script>
    <script src="{{ asset('admin/js/mdb.js') }}"></script>
    <script src="{{ asset('admin/js/addons/datatables.min.js') }}"></script>
    <script src="{{ asset('admin/js/addons/datatables-select.min.js') }}"></script>
    <script src="{{ asset('admin/js/script.js') }}"></script>
    <script src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
    @stack('js')
    @if($errors->any())
        <script>
            {!! implode('', $errors->all('toastr.error(":message");')) !!}
        </script>
    @endif
    @if(session()->has('message'))
        <script>
            toastr.success('{{ session()->get('message') }}');
        </script>
    @endif


</body>
</html>
