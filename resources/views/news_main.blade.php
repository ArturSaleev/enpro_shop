@extends('layouts.app')

@section('content')
    <div class="py-3">
        <div class="container-fluid" style="
            background-color: #EBF7FF;
            background-position: left;
            min-height: 20vw;
            ">
            {!! $new->content_front !!}
        </div>
    </div>
    @include('components.blocks.news')
@endsection
