@extends('layouts.app')

@section('content')
    <div class="mt-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h3>{{ $product->name }}</h3>
                </div>
                <div class="col-lg-6 ">
                    <div class="float-end">
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#orderModal">
                            {{ __('all.buttonbuy') }}
                        </button>
                    </div><br>
                </div>
            </div>
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="tab" href="#t1">{{ __('all.tabsproduct1') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#t2">{{ __('all.tabsproduct2') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#t3">{{ __('all.tabsproduct3') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#t4">{{ __('all.tabsproduct4') }}</a>
                </li>
            </ul>
        </div>

        <div class="py-3">
            <div id="myTabContent" class="tab-content py-4" style="background-color: #EBF7FF;background-position: left;min-height: 20vw;">
                <div class="tab-pane fade show active" id="t1">
                    {!! $product->description_front !!}
                </div>
                <div class="tab-pane fade" id="t2">
                    {!! $product->requirement_front !!}
                </div>
                <div class="tab-pane fade" id="t3">
                    {!! $product->version_front !!}
                </div>
                <div class="tab-pane fade" id="t4">
                    {!! $product->buy_description_front !!}
                </div>
            </div>
        </div>
    </div>


    @include('components.blocks.stocks')
    @include('components.blocks.block_left_img')
    @include('components.blocks.news')
    <!-- Modal -->
    <div class="modal fade" id="orderModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: 1px solid #ffffff !important;">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('all.buttonbuy') }} {{ $product->name }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-6">
                    <form method="POST" action="{{ url("/orders") }}" id="ordercreate">
                        <div class="mb-3">
                            <input type="text" class="form-control" id="version" name="version" placeholder="{{ __('all.orderversion') }}">
                        </div>
                        <div class="mb-3">
                            <input type="number" class="form-control" id="quantity" name="quantity" placeholder="{{ __('all.quantityversion') }}">
                        </div>
                        <div class="mb-3">
                            <input type="text" class="form-control" id="details" name="details" placeholder="{{ __('all.detailsversion') }}">
                        </div>
                        <div class="mb-3">
                            <input type="text" class="form-control" id="contact" name="contact" placeholder="{{ __('all.contactversion') }}">
                        </div>
                        <div class="mb-3">
                            <input type="hidden" class="form-control" id="product_id" name="product_id" value="{{ $product->id }}">
                        </div>
                        <div class="mb-3 text-center">
                            <button type="submit" data-bs-dismiss="modal" class="btn btn-primary" style="background-color: #C4C4C4;border-color: #C4C4C4;">{{ __('all.buttonbuy') }}</button>
                        </div>
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


