@extends('layouts.app')

@section('content')
    @include('components.searchbar')
    <div class="container">
        <div class="row py-3">
            <div class="col-lg-12">
            <a class="link-primary float-end" href="{{ asset('products') }}">{{ __('all.resethardfilter') }}</a>
                <h3>{{ __('all.filterheader') }}</h3>
            </div>
            @foreach($categories as $c)
                @if ($c->parent_id === 0)
                    <div class="col-lg-12 mb-3">
                        <h4 class="mb-3 mt-3">{{ $c->name }}</h4>
                        <a class="btn btn-primary" href="{{ asset('products') }}">{{ __('all.resetfilter') }}</a>
                        @foreach($categories as $c2)
                            @if ($c->id === $c2->parent_id)
                                @isset($c2->active)
                                    <a class="btn btn-primary active" href='{{ url("products/filter/$c2->id") }}'>{{ $c2->name }}</a>
                                @else
                                    <a class="btn btn-primary" href='{{ url("products/filter/$c2->id") }}'>{{ $c2->name }}</a>
                                @endisset
                            @endif
                        @endforeach
                    </div>
                @endif
            @endforeach
        </div>
        <div class="row py-3">
            @foreach($products as $p)
                <div class="col-lg-4 col-12 text-center mb-5">
                    <p><img src="{{ asset($p->image_main) }}" width="70%"></p>
                    <p><h4>{{ $p->name }}</h4></p>
                    <p style="height: 75px">{{ $p->meta }}</p><br>
                    <a href="{{ asset("products/$p->id") }}" class="btn btn-primary">{{ __('all.details') }}</a>
                </div>
            @endforeach
        </div>
    </div>
    @include('components.blocks.stocks')
    @include('components.blocks.news')
@endsection
