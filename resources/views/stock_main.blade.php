@extends('layouts.app')

@section('content')
    <div class="py-3">
        <div class="container-fluid" style="
            background-color: #EBF7FF;
            background-position: left;
            min-height: 20vw;
            ">
            <div class="container">
                {!! $promotion->content_front !!}
            </div>
        </div>
    </div>
    @include('components.blocks.stocks')
@endsection
