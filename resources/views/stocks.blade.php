@extends('layouts.app')

@section('content')
    @include('components.searchbar')
    <div class="container py-6">
        <div class="row">
            @foreach($promotions as $p)
                <div class="col-lg-4 text-center mb-4">
                    <p><img src="{{ asset($p->image)  }}" width="70%"></p>
                    <p><h4>{{ $p->title }}</h4></p><br>
                    <a href="{{ asset("promotions/$p->id") }}" class="btn btn-primary">{{ __('all.details') }}</a>
                </div>
            @endforeach
        </div>
    </div>
@endsection
