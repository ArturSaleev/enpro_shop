@extends('layouts.app')

@section('content')
    @include('components.searchbar')
    @include('components.blocks.banner')
    @include('components.other')
    @include('components.blocks.slider')
    @include('components.blocks.clients')
    @include('components.blocks.stocks')
    @include('components.blocks.block_left_img')
    @include('components.blocks.news')
@endsection
