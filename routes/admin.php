<?php

use App\Http\Controllers\Admin\AttributeController;
use App\Http\Controllers\Admin\BrandsController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ClientsController;
use App\Http\Controllers\Admin\ConfigController;
use App\Http\Controllers\Admin\CurrencyController;
use App\Http\Controllers\Admin\LocaleController;
use App\Http\Controllers\Admin\MainController;
use App\Http\Controllers\Admin\NewsController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\PromotionController;
use App\Http\Controllers\Admin\BannerController;
use Illuminate\Support\Facades\Route;

Route::prefix('admin')
    ->name('admin.')
    ->middleware('auth')
    ->group(function () {
    Route::get('home', [MainController::class, 'index'])->name('home');

    Route::resource('news', NewsController::class);
    Route::resource('language', LocaleController::class);
    Route::resource('clients', ClientsController::class);
    Route::resource('config', ConfigController::class);
    Route::resource('currency', CurrencyController::class);
    Route::get('convert_currency', [CurrencyController::class, 'ServiceConvertCurrency'])->name('convert_currency');
    Route::resource('categories', CategoryController::class);
    Route::resource('brands', BrandsController::class);
    Route::resource('attributes', AttributeController::class);
    Route::resource('products', ProductController::class);
    Route::resource('order', OrderController::class);
    Route::resource('promotions', PromotionController::class);
    Route::resource('banners', BannerController::class);
});
