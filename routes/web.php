<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use UniSharp\LaravelFilemanager\Lfm;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PromotionController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\ForgotPasswordController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['LanguageMiddleware'])->group(function () {

    Route::get('/', [HomeController::class, 'index']);

    Route::get('/lang/{locale}', function ($locale) {
        if (!in_array($locale, ['en', 'ru'])) {
            abort(404);
        }
        session()->put('locale', $locale);
        return back();
    });

    Route::resource('products', ProductController::class);
    Route::get('products/filter/{filter}', [ProductController::class, 'filter']);
    Route::post('products/search', [ProductController::class, 'search']);

    Route::resource('about', AboutController::class);

    Route::resource('news', NewsController::class);

    Route::resource('promotions', PromotionController::class);

    Route::resource('orders', OrderController::class);

    /*Auth::routes([
        'login'    => false, // I ve mafe authorization via API - Evgeniy Tsoy
        'logout'   => true, // I will delete token on frontend - Evgeniy Tsoy
        'register' => true,
        'reset'    => true,   // for resetting passwords
        'confirm'  => false,  // for additional password confirmations
        'verify'   => false,  // for email verification
    ]);*/


// Authentication Routes...
    Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
    Route::post('login', [LoginController::class, 'login']);
    Route::post('logout', [LoginController::class, 'logout'])->name('logout');

// Registration Routes...
    Route::get('register', [RegisterController::class, 'showRegistrationForm'])->name('register');
    Route::post('register', [RegisterController::class, 'register']);

// Password Reset Routes...
    Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
    Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
    Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
    Route::post('password/reset', [ResetPasswordController::class, 'reset'])->name('password.update');

//Route::get('/admin/home', [\App\Http\Controllers\HomeController::class, 'index']);



    Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
        Lfm::routes();
    });


});
